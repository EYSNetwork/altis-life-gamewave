disableSerialization;
_listboxPlayers = (findDisplay 16000) displayCtrl 16001;
_listboxActions = (findDisplay 16000) displayCtrl 16002;

_player = call compileFinal (_listboxPlayers lbData (lbCurSel _listboxPlayers));

if (isNil "AS_godmode") then { // SETUP BASIC VARS
	AS_godmode = false;
};

_vehicles = [
	"B_QuadBike_01_F",
	"C_Hatchback_01_F",
	"C_Offroad_01_F",
	"B_G_Offroad_01_F",
	"C_SUV_01_F",
	"C_Van_01_transport_F",
	"C_Hatchback_01_sport_F",
	"C_Van_01_fuel_F",
	"I_Heli_Transport_02_F",
	"C_Van_01_box_F",
	"I_Truck_02_transport_F",
	"I_Truck_02_covered_F",
	"B_Truck_01_transport_F",
	"B_Truck_01_box_F",
	"O_MRAP_02_F",
	"B_Heli_Light_01_F",
	"O_Heli_Light_02_unarmed_F",
	"C_Rubberboat",
	"C_Boat_Civil_01_F",
	"B_Boat_Transport_01_F",
	"C_Boat_Civil_01_police_F",
	"B_Boat_Armed_01_minigun_F",
	"B_SDV_01_F",
	"B_MRAP_01_F"
];

if ((_listboxActions lbText (lbCurSel _listboxActions)) in _vehicles AND (_listboxActions lbText (lbCurSel _listboxActions)) != "") exitWith
{
	_pos = position player findEmptyPosition [0,50];
	if (count _pos > 0) then {
		closeDialog 0;
		hint parseText format["<t size='1'>Votre carosse vous attend !</t>"];
		(_listboxActions lbText (lbCurSel _listboxActions)) createVehicle position player;
	} else { hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il n'y a pas assez de place pour le véhicule !</t>"];  };
};

switch (_listboxActions lbText (lbCurSel _listboxActions)) do
{
	case "Invulnérabilité ON/OFF":
	{
		if (AS_godmode) then {
			AS_godmode = false;
			player allowDamage true;
			hint parseText format["<t size='2'><t color='#ff0000'>Invulnérabilité OFF</t></t>"];
		} else {
			AS_godmode = true;
			player allowDamage false;
			hint parseText format["<t size='2'><t color='#008000'>Invulnérabilité ON</t></t>"];
		};
	};

	case "Se téléporter à un joueur (sans véhicule)":
	{
		if (isNil "_player") exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Séléctionnez un joueur ! ! </t>"];};
		_pos = position _player findEmptyPosition [0,10];
		if (count _pos > 0) then {
			player setPos _pos;
		} else { hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il n'y a pas la place pour vous téléporter !</t>"];};
	};

	case "Se téléporter à un joueur (avec véhicule)":
	{
		if (isNil "_player") exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Séléctionnez un joueur ! ! </t>"];};
		_pos = position _player findEmptyPosition [0,10];
		if (count _pos > 0) then {
			vehicle player setPos _pos;
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il n'y a pas assez de place pour le véhicule !</t>"]; };
	};

	case "Téléporter un joueur vers moi (sans véhicule)":
	{
		if (isNil "_player") exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Séléctionnez un joueur ! ! </t>"];};
		_pos = position player findEmptyPosition [0,10];
		if (count _pos > 0) then {
			_player setPos _pos;
		} else { hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il n'y a pas la place pour téléporter un joueur ! </t>"];};
	};

	case "Téléporter un joueur vers moi (avec véhicule)":
	{
		if (isNil "_player") exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Séléctionnez un joueur ! ! </t>"];};
		_pos = position player findEmptyPosition [0,10];
		if (count _pos > 0) then {
			vehicle _player setPos _pos;
		} else { hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il n'y a pas assez de place pour le véhicule !</t>"];};
	};

	case "Invulnérabilité véhicule ON/OFF":
	{
		if (vehicle player != player) then
		{
			if (isNil {vehicle player getVariable "AS_godmode"}) then {vehicle player setVariable ["AS_godmode",false,true];};
			if (vehicle player getVariable "AS_godmode") then
			{
				vehicle player setVariable ["AS_godmode",false,true];
				vehicle player allowDamage true;
				hint parseText format["<t size='2'><t color='#ff0000'>Invulnérabilité pour votre véhicule OFF</t></t>"];
			} else {
				vehicle player setVariable ["AS_godmode",true,true];
				vehicle player allowDamage false;
				hint parseText format["<t size='2'><t color='#008000'>Invulnérabilité pour votre véhicule ON</t></t>"];
			};
		} else { hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous n'êtes pas dans un véhicule !</t>"];};
	};

	case "Se téléporter":
	{
		openMap true;AS_wait = true;
		closeDialog 0;
		onMapSingleClick "vehicle player setPos _pos;AS_wait = false;";
		waitUntil {!AS_wait};
		onMapSingleClick "";
		openMap false;
		hint parseText format ["<t size='2'><t color='#ffff32'>Téléportation :</t></t> <br/><t size='1'>Vous vous êtes téléporté aux coordonnées: %1</t>",getPos player];
};
case "Arsenal":
	{
	hint parseText format["<t size='3'>EN DEV FDP</t>"];
	};
	case "Obtenir les clés du véhicule pointé":
	{
		if (!isNull cursorTarget) then {
			life_vehicles set[count life_vehicles,cursorTarget];
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
		hint parseText format["<t size='1'>Vous possédez désormais les clés !</t>"];
	};

	case "Supprimer le véhicule pointé":
	{
		if (!isNull cursorTarget) then {
			deleteVehicle cursorTarget;
			hint parseText format["<t size='2'><t color='#ff0000'>Objet supprimé !</t></t>"];
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
	};

	case "Soigner/Réparer l'objet pointé":
	{
		if (!isNull cursorTarget) then {
			cursorTarget setDamage 0;
			if (cursorTarget isKindOf "LandVehicle") then {cursorTarget setFuel 1;};
				hint parseText format["<t size='2'><t color='#008000'>Soins/Réparation réussite !</t></t>"];
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
	};

	case "Tuer/Exploser l'objet pointé":
	{
		if (!isNull cursorTarget) then {
			cursorTarget setDamage 1;
			hint parseText format["<t size='2'><t color='#ff0000'>Objet explosé/tué !</t></t>"];
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
	};

	case "Réanimer le joueur pointé":
	{
		if (!isNull cursorTarget) then {
			[[name player],"life_fnc_revived", cursorTarget,false] spawn life_fnc_MP;
			hint parseText format["<t size='2'><t color='#008000'>Joueur réanimé !</t></t>"];
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
	};

	case "Ouvrir le coffre du véhicule pointé/actuel":
	{
		if (vehicle player == player) then {
			if (!isNull cursorTarget) then {
				closeDialog 0;sleep 0.01;[cursorTarget] call life_fnc_openInventory;
				hint parseText format["<t size='1.5'>Le coffre du véhicule que vous pointez est ouvert !</t>"];
			} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
		} else {
			closeDialog 0;sleep 0.01;[vehicle player] call life_fnc_openInventory;
			hint parseText format["<t size='1'>Le coffre du véhicule actuel est ouvert !</t>"];
		};
	};

	case "Vérouiller/Dévérouiller la maison/le véhicule":
	{
		_veh = objNull;
		if (vehicle player == player) then {
			_veh = cursorTarget;
		} else {
			_veh = vehicle player;
		};
		if (!isNull _veh) then {
			if(_veh isKindOf "House_F" && playerSide == civilian) then {
				if(player distance _veh < 8) then {
					_door = [_veh] call life_fnc_nearestDoor;
					if(_door == 0) exitWith {hint localize "STR_House_Door_NotNear"};
					_locked = _veh getVariable [format["bis_disabled_Door_%1",_door],0];
					if(_locked == 0) then {
						_veh setVariable[format["bis_disabled_Door_%1",_door],1,true];
						_veh animate [format["door_%1_rot",_door],0];
						systemChat localize "STR_House_Door_Lock";
					} else {
						_veh setVariable[format["bis_disabled_Door_%1",_door],0,true];
						_veh animate [format["door_%1_rot",_door],1];
						systemChat localize "STR_House_Door_Unlock";
					};
				};
			} else {
				_locked = locked _veh;
				if(player distance _veh < 8) then {
					if(_locked == 2) then {
						if(local _veh) then {
							_veh lock 0;
						} else {
							[[_veh,0],"life_fnc_lockVehicle",_veh,false] spawn life_fnc_MP;
						};
						systemChat localize "STR_MISC_VehUnlock";
					} else {
						if(local _veh) then {
							_veh lock 2;
						} else {
							[[_veh,2],"life_fnc_lockVehicle",_veh,false] spawn life_fnc_MP;
						};
						systemChat localize "STR_MISC_VehLock";
					};
				};
			};
		} else {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous ne pointez aucun objet ! </t>"];};
	};

	case "Add $100.000":
	{
		life_cash = life_cash + 100000;
		hint parseText format["<t size='1.5'>+ 100.000€</t>"];
	};
	case "Add $1.000.000":
	{
		life_cash = life_cash + 1000000;
		hint parseText format["<t size='1.5'>+ 1.000.000€</t>"];
	};
	case "Add $100.000.000":
	{
		life_cash = life_cash + 100000000;
		hint parseText format["<t size='1.5'>+ 100.000.000</t>"];
	};

	case "Garage admin":
	{
		lbClear _listboxActions;
		{ _listboxActions lbAdd _x} forEach _vehicles;
		_listboxActions lbAdd "";
		_listboxActions lbAdd "<< BACK";
	};

	case "Spectateur":
	{
		closeDialog 0;[player] execVM "AS_AdminMenu\specta.sqf";
	};

	case "Refund":
	{
		if (!isNil "_player") then {
			AS_toRefund = _player;
			closeDialog 0;
			createDialog "AS_Refund";
			hint format ["Please type the amount you want to refund (Selected Player %1",name _player];
		};
	};

	case "Rafraichir":
	{
		closeDialog 0;
		[] spawn life_fnc_openMenu;
	};

	case "<< BACK":
	{
		closeDialog 0;
		[] spawn life_fnc_openMenu;
	};
};