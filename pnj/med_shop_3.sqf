/* 
	_null=this execVM "pnj\med_shop_3.sqf";
	init="_null=this execVM ""pnj\dealer.sqf"";";
*/
_this enableSimulation false; 
_this allowDamage false; 

_this addAction["<t color='#AAF200'>Magasin d'uniforme</t>",life_fnc_clothingMenu,"medic",100,false,false,"",'playerSide == civilian && license_civ_medic'];
_this addAction["<t color='#AAF200'>Magasin médecin</t>",life_fnc_weaponShopMenu,"med_basic",92,false,false,"",'playerSide == civilian && license_civ_medic'];
_this addAction["<t color='#AAF200'>Concessionnaire médecin</t>",life_fnc_vehicleShopMenu,["med_shop",civilian,"med_car_3","medic","Hopital"],90,false,false,"",'playerSide == civilian && license_civ_medic'];  
_this addAction["<t color='#AAF200'>Héliport médecin</t>",life_fnc_vehicleShopMenu,["med_air_hs",civilian,"medic_spawn_3","medic","Hopital"],90,false,false,"",'playerSide == civilian && license_civ_medic'];  


_this addAction["<t color='#FF9900'>Garage Terrestre</t>",  {  
	[[getPlayerUID player,playerSide,"Car",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;   
	createDialog "Life_impound_menu";
	disableSerialization; 
	ctrlSetText[2802,"Recherches des véhicules...."]; 
	life_garage_sp = "med_car_3"; life_garage_type = "Car";  
	},"",85,false,false,"",'playerSide == civilian && license_civ_medic'];  

_this addAction["<t color='#FF9900'>Garage Aérien</t>",  {  
	 [[getPlayerUID player,playerSide,"Air",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP; 
	 createDialog "Life_impound_menu";
	 disableSerialization;  
		ctrlSetText[2802,"Recherches des véhicules...."];
	 life_garage_sp = "medic_spawn_3";
	 life_garage_type = "Air";
	},"",80,false,false,"",'playerSide == civilian && license_civ_medic'];  
	
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",75,false,false,"",'!life_garage_store && license_civ_medic'];

_this setObjectTexture [0,"texture\skins\medic\medic_uniform.jpg"];


