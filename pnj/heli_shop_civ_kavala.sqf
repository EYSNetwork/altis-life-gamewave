/* 
	CarshopLucel
*/
removeallWeapons _this; 
_this enableSimulation false; 
_this allowDamage false; 
_this addAction["<t color='#AAF200'>Héliport civil</t>",
life_fnc_vehicleShopMenu,["civ_air",civilian,["heli_kavala","heli_kavala_1","heli_kavala_2","heli_kavala_3","heli_kavala_4"]
,"civ","Héliport civil"]];

_this addAction["<t color='#AAF200'>Héliport dépanneur</t>",
life_fnc_vehicleShopMenu,["dep_air",civilian,["heli_kavala","heli_kavala_1","heli_kavala_2","heli_kavala_3","heli_kavala_4"]
,"dep","Héliport"],90,false,false,"",'playerSide == civilian && license_civ_dep'];  



_this addAction["<t color='#FF9900'>Garage</t>",
{  [[getPlayerUID player,playerSide,"Air",player],"TON_fnc_getVehicles",false,false] spawn life_fnc_MP;
life_garage_type = "Air";
createDialog "Life_impound_menu";
disableSerialization;
ctrlSetText[2802,"Recherches des véhicules...."];
life_garage_sp = "heli_kavala";  }];
_this addAction["<t color='#FF9900'>Rentrer au garage</t>",life_fnc_storeVehicle,"",0,false,false,"",'!life_garage_store'];