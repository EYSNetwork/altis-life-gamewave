/* 
	_null=this execVM "pnj\camera_banque.sqf";
*/
_this enableSimulation false; 
_this allowDamage false; 
_this addAction["<t color='#AAF200'>Banque fédérale - Entrée principal</t>",life_fnc_fedCamDisplay,"front"]; 
_this addAction["<t color='#AAF200'>Banque fédérale - Entrée secondaire</t>",life_fnc_fedCamDisplay,"side"];  
_this addAction["<t color='#AAF200'>Banque fédérale - Entrée arrière</t>",life_fnc_fedCamDisplay,"back"];  
_this addAction["<t color='#AAF200'>Banque fédérale - Vue de la salle des coffres</t>",life_fnc_fedCamDisplay,"vault"];
_this addAction["<t color='#FF9900'>Eteindre l'écran</t>",life_fnc_fedCamDisplay,"off"];