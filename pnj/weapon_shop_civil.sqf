/* 
	Lucel
	_null=this execVM "pnj\weapon_shop_civil.sqf";
*/

_this enableSimulation false; 
_this allowDamage false; 

_this addAction["<t color='#FF9900'>Armurerie civil</t>",life_fnc_weaponShopMenu,"gun",0,false,false,"",' license_civ_gun && playerSide == civilian'];

_this addAction[format["<t color='#00ffff'>%1</t> <t color='#AAF200'> (%2€) </t>",["license_civ_gun"] call life_fnc_varToStr,[(["gun"] call life_fnc_licensePrice)] call life_fnc_numberText],life_fnc_buyLicense,"gun",0,false,false,"",' !license_civ_gun && playerSide == civilian '];
