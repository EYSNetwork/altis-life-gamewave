/* 
	licensesShop
	_null=this execVM "pnj\Licenses_shop.sqf";
*/
// addAction args: title, filename, (arguments, priority, showWindow, hideOnUse, shortcut, condition, positionInModel, radius, radiusView, showIn3D, available, textDefault, textToolTip)


_this allowDamage false;  
_this enableSimulation false; 
_this setVariable["realname", "Gestion des permis"];

///////////// 
//////ACHAT//
/////////////
//Driver
_this addAction[format["<t color='#00ffff'>Achat: %1</t>  <t color='#AAF200'>(%2€)</t>",["license_civ_driver"] call life_fnc_varToStr,[(["driver"] call life_fnc_licensePrice)] call life_fnc_numberText],
life_fnc_buyLicense,"driver",0,false,false,"",' !license_civ_driver && playerSide == civilian '];
//Boat
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_boat"] call life_fnc_varToStr,[(["boat"] call life_fnc_licensePrice)] call life_fnc_numberText],
life_fnc_buyLicense,"boat",0,false,false,"",' !license_civ_boat && playerSide == civilian '];
//Air
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_air"] call life_fnc_varToStr,[(["pilot"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_buyLicense,"pilot",0,false,false,"",' !license_civ_air && playerSide == civilian '];
//Truck
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_truck"] call life_fnc_varToStr,[(["truck"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_buyLicense,"truck",0,false,false,"",' !license_civ_truck && playerSide == civilian '];
//House
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€))</t>",["license_civ_home"] call life_fnc_varToStr,[(["home"] call life_fnc_licensePrice)]
 call life_fnc_numberText],life_fnc_buyLicense,"home",0,false,false,"",' !license_civ_home && playerSide == civilian '];
 //Dive
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€))</t>",["license_civ_dive"] call life_fnc_varToStr,[(["dive"] call life_fnc_licensePrice)]
 call life_fnc_numberText],life_fnc_buyLicense,"dive",0,false,false,"",' !license_civ_dive && playerSide == civilian '];
 
 //METIER
 //Taxi
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€))</t>",["license_civ_taxi"] call life_fnc_varToStr,[(["taxi"] call life_fnc_licensePrice)]
 call life_fnc_numberText],life_fnc_buyLicense,"taxi",0,false,false,"",' !license_civ_taxi && !license_civ_rebel && !license_civ_merc && playerSide == civilian '];
//Dep  
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_dep"] call life_fnc_varToStr,[(["dep"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_buyLicense,"dep",0,false,false,"",' !license_civ_dep && !license_civ_rebel && !license_civ_merc && playerSide == civilian '];
//Avocats 
_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_avocat"] call life_fnc_varToStr,[(["avocat"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_buyLicense,"avocat",0,false,false,"",' !license_civ_avocat && !license_civ_rebel && !license_civ_merc && playerSide == civilian '];
  
  
//Mercenaire  
//_this addAction[format["<t color='#00ffff'>Achat: %1</t> <t color='#AAF200'>(%2€)</t>",["license_civ_merc"] call life_fnc_varToStr,[(["merc"] call life_fnc_licensePrice)] call life_fnc_numberText],
//  life_fnc_buyLicense,"merc",0,false,false,"",' !license_civ_dep && !license_civ_medic && !license_civ_taxi && !license_civ_rebel && playerSide == civilian '];
  
 
 
///////////// 
//////VENTE//
/////////////
//Driver
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_driver"] call life_fnc_varToStr,[(["driver"] call life_fnc_licensePrice)] call life_fnc_numberText],
life_fnc_sellLicense,"driver",0,false,false,"",' license_civ_driver && playerSide == civilian '];
//Boat
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_boat"] call life_fnc_varToStr,[(["boat"] call life_fnc_licensePrice)] call life_fnc_numberText],
life_fnc_sellLicense,"boat",0,false,false,"",' license_civ_boat && playerSide == civilian '];
//Air
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_air"] call life_fnc_varToStr,[(["pilot"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_sellLicense,"pilot",0,false,false,"",' license_civ_air && playerSide == civilian '];
//Truck
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_truck"] call life_fnc_varToStr,[(["truck"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_sellLicense,"truck",0,false,false,"",' license_civ_truck && playerSide == civilian '];
//Dep  
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_dep"] call life_fnc_varToStr,[(["dep"] call life_fnc_licensePrice)] call life_fnc_numberText],
  life_fnc_sellLicense,"dep",0,false,false,"",' license_civ_dep && playerSide == civilian '];
//House
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_home"] call life_fnc_varToStr,[(["home"] call life_fnc_licensePrice)]call life_fnc_numberText],
life_fnc_sellLicense,"home",0,false,false,"",' license_civ_home && playerSide == civilian '];
//Taxi
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_taxi"] call life_fnc_varToStr,[(["taxi"] call life_fnc_licensePrice)]call life_fnc_numberText],
life_fnc_sellLicense,"taxi",0,false,false,"",' license_civ_taxi && playerSide == civilian '];

//Dive
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_dive"] call life_fnc_varToStr,[(["dive"] call life_fnc_licensePrice)]call life_fnc_numberText],
life_fnc_sellLicense,"dive",0,false,false,"",' license_civ_dive && playerSide == civilian '];
//Merc
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_merc"] call life_fnc_varToStr,[(["merc"] call life_fnc_licensePrice)]call life_fnc_numberText],
life_fnc_sellLicense,"merc",0,false,false,"",' license_civ_merc && playerSide == civilian '];
//Avocat
_this addAction[format["<t color='#AAF200'>Vendre: </t> <t color='#00ffff'>%1</t>",["license_civ_avocat"] call life_fnc_varToStr,[(["avocat"] call life_fnc_licensePrice)]call life_fnc_numberText],
life_fnc_sellLicense,"avocat",0,false,false,"",' license_civ_avocat && playerSide == civilian '];
