/*
	TrackerLucel
	POMME
	altislife.fr
*/
private["_curTarget","_distance","_isVehicle","_title","_progressBar","_cP","_titleText","_dice","_badDistance","_markers","_uid","_name","_pid","_unit","_index"];
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
_curTarget = cursorTarget;
_pid = getPlayerUID player;
_unit = player;
_name = name _unit;

life_interrupted = false;
if(life_action_inUse) exitWith {};
if(isNull _curTarget) exitWith {}; //Bad type
_distance = ((boundingBox _curTarget select 1) select 0) + 2;
if(player distance _curTarget > _distance) exitWith {}; //Too far
_isVehicle = if((_curTarget isKindOf "LandVehicle") OR (_curTarget isKindOf "Ship") OR (_curTarget isKindOf "Air")) then {true} else {false};
//if(_isVehicle && _curTarget in life_vehicles_c4ed) exitWith {hint "Ce véhicule est déja piégé..."};

//More error checks
if(!_isVehicle && !isPlayer _curTarget) exitWith {};
if(!_isVehicle && !(_curTarget getVariable["restrained",false])) exitWith {};

_title = format["Mise en place du C4 sur %1",getText(configFile >> "CfgVehicles" >> (typeOf _curTarget) >> "displayName")];
life_action_inUse = true; //Lock out other actions


//Setup the progress bar
disableSerialization;
5 cutRsc ["life_progress","PLAIN"];
_ui = uiNamespace getVariable "life_progress";
_progressBar = _ui displayCtrl 38201;
_titleText = _ui displayCtrl 38202;
_titleText ctrlSetText format["%2 (1%1)...","%",_title];
_progressBar progressSetPosition 0.01;
_cP = 0.05;
//_cP = 0.01;

while {true} do
{
	if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
		[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
		player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
	};
	sleep 0.25;
	_cP = _cP + 0.1;
	_progressBar progressSetPosition _cP;
	_titleText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_title];
	if(_cP >= 1 OR !alive player) exitWith {};
	if(life_istazed) exitWith {}; //Tazed
	if(life_interrupted) exitWith {};
	if((player getVariable["restrained",false])) exitWith {};
	if(player distance _curTarget > _distance) exitWith {_badDistance = true;};
};

//Kill the UI display and check for various states
5 cutText ["","PLAIN"];
player playActionNow "stop";
if(!alive player OR life_istazed) exitWith {life_action_inUse = false;};
if((player getVariable["restrained",false])) exitWith {life_action_inUse = false;};
if(!isNil "_badDistance") exitWith {titleText["Vous êtes trop loin de la cible","PLAIN"]; life_action_inUse = false;};
if(life_interrupted) exitWith {life_interrupted = false; titleText["Action annulé","PLAIN"]; life_action_inUse = false;};

diag_log format ["C401 || _curTarget: %1 _unit :%2", _curTarget, _unit];


if (isNull _curTarget ) exitWith {};

	_owners = _curTarget getVariable ["vehicle_info_c4",[]];
	if([_pid,name player] in _owners) then {
	systemChat format["C4 déja installé. Vous avez déja placé un C4 sur ce véhicule."];
	hint parseText format["<t size='3'><t color='#00FF00'>C4 déja installé</t></t> <br/><t size='1.5'>Vous avez déja placé un C4 sur ce véhicule.</t>"];
	}else{	
    _owners pushBack [_pid,player getVariable["realname",name player]];
	_curTarget setVariable["vehicle_info_c4",_owners,true];
	player setVariable["c4",true];
	systemChat format["C4 installé."];
	hint parseText format["<t size='3'><t color='#00FF00'>C4 installé</t></t> <br/><t size='1.5'>Vous pouvez déclencher la détonation quand bon vous semble</t>"];
	_curTarget setVariable["vehicle_info_c4",_owners,true];
	if(!([false,"blastingcharge",1] call life_fnc_handleInv)) exitWith {life_action_inUse = false;};
	life_vehicles_c4ed pushBack _curTarget;
	life_action_inUse = false;
	 };
life_action_inUse = false;
