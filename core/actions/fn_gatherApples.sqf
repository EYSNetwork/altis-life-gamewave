if(life_action_inUse) exitWith {};
if((animationState player) == "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 6)+1;
_sum = ["apple",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Récolte de Pommes...","PLAIN"];
	titleFadeOut 5;
	for "_i" from 0 to 2 do
	{
		player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
		waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
		sleep 2.5;
		if (_i == 2) then {life_action_inUse = false;};
	};
	if(([true,"apple",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez récolté %1 pommes",_sum],"PLAIN"];
	};
}
else
{
	hint "Ton inventaire est plein !";
};