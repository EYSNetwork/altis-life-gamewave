private["_unit"];


if(side player == west) then {
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _unit OR !(_unit getVariable["restrained",FALSE])) exitWith {}; //Error check?
[[player, "CuffOut",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
_unit playMoveNow "AmovPercMstpSnonWnonDnon";
_unit setVariable["CopRestrain",false,true];
_unit setVariable["restrained",false,true];
_unit setVariable["Escorting",false,true];
_unit setVariable["transporting",false,true];
_unit setVariable["surrender",false,true];
//[_unit] spawn life_fnc_initGang;
[[],"life_fnc_initGang",_unit,false] spawn Life_fnc_MP;
detach _unit;
[[0,format["%1 est libere par %2",name _unit, name player]],"life_fnc_broadcast",west,FALSE] call Life_fnc_MP;

};



if(side player == civilian) then {

//_unit = cursorTarget;
//if(isNull _unit || !(_unit getVariable "restrained")) exitWith {};
//[[_unit],"life_fnc_unrestrainAction",_unit,false] spawn Life_fnc_mp;


_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _unit OR !(_unit getVariable["restrained",FALSE])) exitWith {}; //Error check?
//if(_unit getVariable["Coprestrain"])) exitWith {hint "Vous devez utiliser un outil de crochetage"}; //Error check?
[[player, "CuffOut",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
_unit playMoveNow "AmovPercMstpSnonWnonDnon";
_unit setVariable["CopRestrain",false,true];
_unit setVariable["restrained",false,true];
_unit setVariable["Escorting",false,true];
_unit setVariable["transporting",false,true];
_unit setVariable["surrender",false,true];
//[_unit] spawn life_fnc_initGang;
[[],"life_fnc_initGang",_unit,false] spawn Life_fnc_MP;
detach _unit;


};