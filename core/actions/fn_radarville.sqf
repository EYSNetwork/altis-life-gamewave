// Radar en ville
// Par Georges Monfrere pour Altislife.fr / Gamewave.



_speed = speed player;
_veh = vehicle player;

if((_veh getVariable "SirenMedic")) exitWith {};
if((_veh getVariable "SirenCop")) exitWith {};

if((_speed > 55) && (_speed < 99)) then
{
hint "Vous avez été flashé !!! La limitation est de 50km/h.";
cutText["Vous avez été flashé !","WHITE IN"];
[[getPlayerUID player,name player,"800"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
};

if((_speed > 100) && (_speed < 249)) then
{
hint "Vous avez été flashé ! La limitation est de 50km/h.";
cutText["Vous avez été flashé !","WHITE IN"];
[[getPlayerUID player,name player,"801"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
};

if (_speed > 250) then
{
Hint "RADAR ERROR CALCULATING SPEED ! SPEED TOO HIGH..."
cutText["Vous avez été flashé !","WHITE IN"];
};