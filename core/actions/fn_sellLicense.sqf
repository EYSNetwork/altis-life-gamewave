/*
	File: fn_buyLicense.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Called when purchasing a license. May need to be revised.
*/
private["_type"];
_type = _this select 3;

_price = [_type] call life_fnc_licensePrice;
_license = [_type,0] call life_fnc_licenseType;
_realprice = (_price / 2);
life_flouze = life_flouze + _realprice;
titleText[format[(localize "STR_NOTF_B_2"), _license select 1,[_realprice] call life_fnc_numberText],"PLAIN"];
missionNamespace setVariable[(_license select 0),false];
[0] call SOCK_fnc_updatePartial;
[2] call SOCK_fnc_updatePartial;