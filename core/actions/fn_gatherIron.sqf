if(life_action_inUse) exitWith {};
if((animationState player) == "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon") exitWith {};
if((time - life_action_delay) < 3) exitWith {};
life_action_delay = time;
private["_sum","_rand"];

_rand=floor(random 3)+1;
_sum = ["ironore",_rand,life_carryWeight,life_maxWeight] call life_fnc_calWeightDiff;
if (life_inv_pickaxe < 1) exitWith {hint "Vous avez besoin d'une pioche pour effectuer cette action.";life_action_inUse = false;};
if(_sum > 0) then
{
	life_action_inUse = true;
	titleText["Récolte de Fer...","PLAIN"];
	titleFadeOut 5;
	[[player, "mining",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	for "_i" from 0 to 2 do
	{
		player playMove "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";
		waitUntil{animationState player != "AinvPercMstpSnonWnonDnon_Putdown_AmovPercMstpSnonWnonDnon";};
		sleep 2.5;
		if (_i == 2) then {life_action_inUse = false;};
	};
	if(([true,"ironore",_sum] call life_fnc_handleInv)) then
	{
		titleText[format["Vous avez collecté %1 fer",_sum],"PLAIN"];
	};
};