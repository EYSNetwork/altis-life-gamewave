/*

	File: fn_PALFinish.sqf
	Author: For GameWave
	Paye pour une mission Retour Palettes Pegasus
	
*/

private["_nearVehicles","_vehicle"]; //suppression: ,"_pal"

	_nearVehicles = nearestObjects[getPos (_this select 0),["Car","Air","Ship"],20]; //Recherche de vehicules dans les 20 m
	
	if(count _nearVehicles > 0) then
	{
		{
			if(!isNil "_vehicle") exitWith {}; //Evite les boucles.
			_vehData = _x getVariable["vehicle_info_owners",[]];
			if(count _vehData  > 0) then
			{
				_vehOwner = (_vehData select 0) select 0;
				if((getPlayerUID player) == _vehOwner) exitWith
				{
					_vehicle = _x;
				};
			};
		} foreach _nearVehicles;
	};

if(isNil "_vehicle") exitWith {["MessagePNJ",["T'es venu à pied? Rapproches ton camion qu'on le charge..."]] call BIS_fnc_showNotification };
if(isNull _vehicle) exitWith {};

if (typeOf _vehicle != "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["Il est où ton camion Pegasus? Et les palettes?"]] call BIS_fnc_showNotification
	};
		
_veh = _vehicle;
_color = [(typeOf _veh),(_veh getVariable "Life_VEH_color")] call life_fnc_vehicleColorStr;

if (typeOf _vehicle == "I_Truck_02_box_F") then 
	{
		["MessagePNJ",["C'est quoi ce camion? Elles sont où les Palettes?"]] call BIS_fnc_showNotification
	};

if (typeOf _vehicle == "I_Truck_02_box_F") then
	{
		switch (str(life_pal_start)) do
			{
				case "Pegasus_1":
					{
					private["_pal","_price"];
						_price = round(1 * 15000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_2":
					{
					private["_pal","_price"];
						_price = round(1 * 5000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_3":
					{
					private["_pal","_price"];
						_price = round(1 * 14000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_4":
					{
					private["_pal","_price"];
						_price = round(1 * 13000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_5":
					{
					private["_pal","_price"];
						_price = round(1 * 6000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_6":
					{
					private["_pal","_price"];
						_price = round(1 * 6000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_7":
					{
					private["_pal","_price"];
						_price = round(1 * 18000);
					life_delivery_in_progress = false;
					life_pal_point = nil;	
					_pal = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
			};
	};	
/*
Pegasus1 = Kavala 			$15.000
Pegasus2 = Athira 			$5.000
Pegasus3 = Sofia 			$14.000
Pegasus4 = Panagia			$13.000
Pegasus5 = Zaros			$6.000
Pegasus6 = Agios Dionysios	$6.000
Pegasus7 = Abdera (DP5)		$18.000
*/

	