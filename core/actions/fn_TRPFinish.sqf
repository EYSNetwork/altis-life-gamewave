/*

	File: fn_TRPFinish.sqf
	Author: For GameWave
	Paye pour une mission de livraison Pegasus
	
*/

private["_nearVehicles","_vehicle"]; //suppression: ,"_trp"

	_nearVehicles = nearestObjects[getPos (_this select 0),["Car","Air","Ship"],20]; //Recherche de vehicules dans les 20 m
	
	if(count _nearVehicles > 0) then
	{
		{
			if(!isNil "_vehicle") exitWith {}; //Evite les boucles.
			_vehData = _x getVariable["vehicle_info_owners",[]];
			if(count _vehData  > 0) then
			{
				_vehOwner = (_vehData select 0) select 0;
				if((getPlayerUID player) == _vehOwner) exitWith
				{
					_vehicle = _x;
				};
			};
		} foreach _nearVehicles;
	};

if(isNil "_vehicle") exitWith {["MessagePNJ",["T'es venu à pied? Rapproches ton camion qu'on le charge..."]] call BIS_fnc_showNotification };
if(isNull _vehicle) exitWith {};

if (typeOf _vehicle != "I_Truck_02_box_F") exitWith 
	{
		["MessagePNJ",["Il est où ton camion? T'as pas perdu les colis hein?"]] call BIS_fnc_showNotification
	};
		
_veh = _vehicle;
_color = [(typeOf _veh),(_veh getVariable "Life_VEH_color")] call life_fnc_vehicleColorStr;

/*
if (typeOf _vehicle == "I_Truck_02_box_F") exitWith 
	{
		["MessagePNJ",["C'est quoi ce camion? Ils sont où les colis?"]] call BIS_fnc_showNotification
	};
*/
if (typeOf _vehicle == "I_Truck_02_box_F") then
	{
		switch (str(life_trp_point)) do
			{
				case "Pegasus_1":
					{
					private["_trp","_price"];
						_price = round(1 * 85000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_2":
					{
					private["_trp","_price"];
						_price = round(1 * 35000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_3":
					{
					private["_trp","_price"];
						_price = round(1 * 90000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_4":
					{
					private["_trp","_price"];
						_price = round(1 * 86000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_5":
					{
					private["_trp","_price"];
						_price = round(1 * 60000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_6":
					{
					private["_trp","_price"];
						_price = round(1 * 80000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
					
				case "Pegasus_7":
					{
					private["_trp","_price"];
						_price = round(1 * 150000);
					life_delivery_in_progress = false;
					life_trp_point = nil;	
					_trp = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
					["DeliverySucceeded",[format[(localize "STR_NOTF_Earned_1"),[_price] call life_fnc_numberText]]] call bis_fnc_showNotification;
					life_cur_task setTaskState "Succeeded";
					player removeSimpleTask life_cur_task;
					life_flouze = life_flouze + _price;
					};
			};
	};	

/*
Pegasus1 = Kavala 			+ Loin - ATM - Rebelles		75.000
Pegasus2 = Athira 			- Loin - ATM - Rebelles		25.000
Pegasus3 = Sofia 			+ Loin - ATM + Rebelles		85.000
Pegasus4 = Panagia			+ Loin + ATM + Rebelles		75.000
Pegasus5 = Zaros			- Loin - ATM - Rebelles		40.000
Pegasus6 = Agios Dionysios	+ Loin + ATM + Rebelles		60.000
Pegasus7 = Abdera (DP5)		+ Loin + ATM - Rebelles		100.000
*/	
	