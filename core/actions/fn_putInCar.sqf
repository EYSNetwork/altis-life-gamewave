/*
	File: fn_putInCar.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Finds the nearest vehicle and loads the target into the vehicle.
*/
private["_unit"];
if(side player == west) then {
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _unit OR !isPlayer _unit) exitWith {};
//Je suis menotté ?
if(player getVariable "restrained") exitWith {};
_nearestVehicle = nearestObjects[getPosATL player,["Car","Ship","Submarine","Air"],10] select 0;
if(isNil "_nearestVehicle") exitWith {hint "Il n'y a pas de véhicules proche..."};

detach _unit;
[[_nearestVehicle],"life_fnc_moveIn",_unit,false] call life_fnc_MP;
_unit setVariable["Escorting",FALSE,TRUE];
_unit setVariable["transporting",TRUE,TRUE];
closeDialog 0;
};
/*
if(side player == civilian) then {
_unit = cursorTarget;
if(isNull _unit OR !isPlayer _unit) exitWith {};
_near = (position player) nearEntities [["Man"],2];
_nearest = _near select 0;

if(isNull _nearest) exitWith {}; //Not valid
if(!(_nearest getVariable "restrained")) exitwith {};
detach _nearest;
_nearest action ["getInCargo", _unit];
_nearest setVariable["Escorting",false,true];
_nearest setVariable["transporting",true,true];
player setVariable["currentlyEscorting",false,true];


};
*/


if(side player == civilian) then {
_unit = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;
if(isNull _unit OR !isPlayer _unit) exitWith {};
//Je suis menotté ?
if(player getVariable "restrained") exitWith {};
//La cible est menotté par un policier ?
if(_unit getVariable "CopRestrain") exitWith {};

_nearestVehicle = nearestObjects[getPosATL player,["Car","Ship","Submarine","Air"],10] select 0;
if(isNil "_nearestVehicle") exitWith {hint "Il n'y a pas de véhicules proche..."};

detach _unit;
[[_nearestVehicle],"life_fnc_moveIn",_unit,false] call life_fnc_MP;
_unit setVariable["Escorting",FALSE,TRUE];
_unit setVariable["transporting",TRUE,TRUE];
closeDialog 0;

};