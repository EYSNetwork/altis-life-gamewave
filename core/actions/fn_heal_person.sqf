/*
	Heal person lucel
*/
private["_curTarget"];
_curTarget = cursortarget;
if(!license_civ_medic) exitWith {hint "Vous n'êtes pas médecin."};
if((damage _curTarget) < 0.25) exitWith {};
if(!("FirstAidKit" in (items player))) exitWith {};
player removeItem "FirstAidKit";
player playMove "AinvPknlMstpSnonWnonDnon_medic_1";
sleep 3;
_curTarget setDamage 0;

