/*
	File: fn_unrestrainAction.sqf
	Author: Skalicon
	
	Description:
	Unrestrains player
*/
private["_unit"];
_unit = [_this,0,Objnull,[Objnull]] call BIS_fnc_param;
if(isNull _unit OR !(_unit getVariable["restrained",FALSE])) exitWith {}; //Error check?
[[player, "CuffOut",10],"life_fnc_playSound",true,false] spawn Life_fnc_MP;
_unit playMoveNow "AmovPercMstpSnonWnonDnon";
_unit setVariable["CopRestrain",false,true];
_unit setVariable["restrained",false,true];
_unit setVariable["Escorting",false,true];
_unit setVariable["transporting",false,true];
_unit setVariable["surrender",false,true];
detach _unit;
[_unit] spawn life_fnc_initGang;
