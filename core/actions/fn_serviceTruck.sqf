/*
	File: fn_serviceTruck.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Main functionality for the service truck.
	*Needs to be revised for new system and flow*
*/
if((time - life_action_delay) < 15) exitWith {hint "Doucement ! Un client à la fois !"};
life_action_delay = time;
private["_nearby"];
_nearby = nearestObjects[(vehicle player),["Car","Ship","Air"],10];
if(count (_nearby) > 1) then
{
	_vehicle = _nearby select 1;
	_name = getText(configFile >> "CfgVehicles" >> (typeOf _vehicle) >> "displayName");
	titleText[format["Entretient du %1 en cours...",_name],"PLAIN"];
	[[player, "RepairTruck",10],"life_fnc_playSound",true,false] spawn life_fnc_MP;
	titleFadeOut 12;
	sleep 15;
	if((vehicle player) distance _vehicle > 10) exitWith {titleText["Vehicule trop loin...","PLAIN"];};
	titleText[format["Tu as rempli/réparé %1",_name],"PLAIN"];
	if(!local _vehicle) then
	{
		[{_vehicle setFuel 1;},"BIS_fnc_spawn",_vehicle,false] spawn life_fnc_MP;
	}
		else
	{
		_vehicle setFuel 1;
	};
	_vehicle setDamage 0;
};