/*
	File: fn_restrainAction.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Retrains the target.
	Bidouiller par LECUL pour Altislife.fr
*/
private["_unit"];
_unit = cursorTarget;

if(isNull _unit) exitWith {}; //Not valid
if((_unit getVariable "restrained")) exitWith {};
if((_unit getVariable "Escorting")) exitWith {};
if(player getVariable "restrained") exitWith {}; // je suis menott� ?
if(player == _unit) exitWith {};
if(!isPlayer _unit) exitWith {};
if((player distance _unit > 3)) exitWith {};

switch (playerSide) do
{
	case west:
		{
			_unit setVariable["CopRestrain",true,true];
			_unit setVariable["restrained",true,true];
			_unit setVariable["surrender",false,true];
			_unit setVariable["robbedmap",false,true];
			_unit setVariable["robbedphone",false,true];
			//if((_unit getVariable["surrender",FALSE])) then { _unit setVariable["surrender",FALSE,TRUE]; _unit switchMove ""; };
			//[[0,format["%1 se fait menotter par %2", name _unit, name player]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
			[[0,format["%1 se fait menotter par %2",_unit getVariable["realname", name _unit], profileName]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
			[[player], "life_fnc_restrain", _unit, false] spawn life_fnc_MP;
		};
	
	case civilian:
		{
		if(life_inv_menotte < 1) exitWith {hint"Vous devez avoir une paire de menottes."};
		life_inv_menotte = life_inv_menotte - 1;
		//_unit setVariable["CopRestrain",false,true];
		_unit setVariable["restrained",true,true];
		_unit setVariable["surrender",false,true];
		_unit setVariable["robbedmap",false,true];
		_unit setVariable["robbedphone",false,true];
		//if((_unit getVariable["surrender",FALSE])) then { _unit setVariable["surrender",FALSE,TRUE]; _unit switchMove ""; };
		[[player], "life_fnc_restrain", _unit, false] spawn life_fnc_MP;
		};
};