/*
	File: fn_zoneCreator.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Creates triggers around the map to add the addAction for specific
	fields such as apples, cocaine, heroin, etc. This method is to reduce
	CPU load.
	
	Note: 
	Triggers are NOT my preferred method so this is considered temporary until a more suitable
	option is presented.

*/
private["_appleZones","_peachZones","_heroinZones","_cocaineZones","_weedZones","_houblonZones"];
_appleZones = ["apple_1","apple_2","apple_3","apple_4","apple_5"];
_peachZones = ["peaches_1","peaches_2","peaches_3","peaches_4"];
_heroinZones = ["heroin_1"];
_cocaineZones = ["cocaine_1","cocaine_2"];
_weedZones = ["weed_1","weed_2"];
_houblonZones = ["houblon_1","houblon_2"];

//_phosZones = ["meth_1"];
_soudeZones = ["meth_2"];

//Create apple zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[25,25,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Apples = player addAction[(""<t color=""""#AAF200"""">Récolter des pommes</t>""),life_fnc_gatherApples,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Apples;"];
} foreach _appleZones;

//Create peach zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[25,25,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Peaches = player addAction[""<t color=""""#AAF200"""">Récolter des peches</t>"",life_fnc_gatherPeaches,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Peaches;"];
} foreach _peachZones;

//Create heroin zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[50,50,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_heroin = player addAction[""<t color=""""#AAF200"""">Récolter du Pavot</t>"",life_fnc_gatherHeroin,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Heroin;"];
} foreach _heroinZones;

//Create Weed zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[50,50,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Cannabis = player addAction[""<t color=""""#AAF200"""">Récolter du cannabis</t>"",life_fnc_gatherCannabis,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Cannabis;"];
} foreach _weedZones;

//Create Houblon zones
{
_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
_zone setTriggerArea[25,25,0,false];
_zone setTriggerActivation["CIV","PRESENT",true];
_zone setTriggerStatements["player in thislist","LIFE_Action_houblon = player addAction[""<t color=""""#AAF200"""">Cueillette du Houblon</t>"",life_fnc_gatherHoublon,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_houblon;"];
} foreach _houblonZones;

//Create cocaine zones
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[50,50,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Coke = player addAction[""<t color=""""#AAF200"""">Récolter Cocaïne</t>"",life_fnc_gatherCocaine,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Coke;"];
} foreach _cocaineZones;

/*
//Create Phos
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[50,50,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Phos = player addAction[""<t color=""""#AAF200"""">Récolter du Phosphore</t>"",life_fnc_gatherPhos,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Phos;"];
} foreach _phosZones;
*/
//Create Soude
{
	_zone = createTrigger ["EmptyDetector",(getMarkerPos _x)];
	_zone setTriggerArea[50,50,0,false];
	_zone setTriggerActivation["CIV","PRESENT",true];
	_zone setTriggerStatements["player in thislist","LIFE_Action_Soude = player addAction[""<t color=""""#AAF200"""">Récolter de la Soude Caustique</t>"",life_fnc_gatherSoude,'',0,false,false,'','!life_action_inUse'];","player removeAction LIFE_Action_Soude;"];
} foreach _soudeZones;
