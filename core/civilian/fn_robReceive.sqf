/*
	File: fn_robReceive.sqf
	Author: Bryan "Tonic" Boardwine
*/
private["_cash"];
_cash = [_this,0,0,[0]] call BIS_fnc_param;
if(_cash == 0) exitWith {titleText["Pas d'argent sur cette personne","PLAIN"]};

life_flouze = life_flouze + _cash;
titleText[format["Vous avez dérobé %1 €",[_cash] call life_fnc_numberText],"PLAIN"];
[6] call SOCK_fnc_updatePartial;