/*
	File: fn_welcomeNotification.sqf
	
	Description:
	Called upon first spawn selection and welcomes our player.

format["Bienvenue %1",name player] hintC
[
	"Site Web : http://www.altislife.fr",
	"TeamSpeak : ts.gamewave.fr",
	"Vous jouez sur la mission Altis Life RPG 3.1.3 par Tonic et modifiée par Altislife.fr",
	"Menu d'intéraction (Windows Gauche) : Réparer le véhicule / Réanimation / Ramasser l'argent, Tortue... / Menu Police",
	"MAJ + G : Se rendre",
	"MAJ + F : Assommer un joueur",
	"MAJ + H / CTRL + H : Ranger / Sortir une arme",
	"MAJ + ESPACE : Sauter",
	"MAJ + R : Menotter (Police)",
	"MAJ + L : Gyrophare (Police)"
	
];

*/
"" hintC parseText "<t><t size='1.5'>Bienvenue sur Altislife France</t><br/>
<br/>
<t>Site Web : </t><t color='#AAF200'>http://www.altislife.fr</t><br/>
<t>TeamSpeak : </t><t color='#AAF200'>ts.gamewave.fr</t><br/>
</t><t color='#FF0000'>Consultez les règles avant de jouer !</t><br/>
<br/>
<t color='#AAF200'>Menu d'intéraction (Windows Gauche) : </t>Menu Métier (Taxi / Médecin / Dépanneur) /<t> Menu maison / Menu véhicule / Démenottage / Escorte / Réanimation / Ramasser des objets(Tortue / Argent) /</t><t color='#21c3ff'>Menu Police</t><br/>
<t color='#AAF200'>U : </t><t>Ouvrir le véhicule / la maison</t><br/>
<t color='#AAF200'>MAJ + G : </t><t>Se rendre</t><br/>
<t color='#AAF200'>MAJ + F : </t><t>Assommer</t><br/>
<t color='#AAF200'>MAJ + H / CTRL + H : </t><t>Ranger / Dégainer l'arme</t><br/>
<t color='#AAF200'>MAJ + ESPACE : </t><t>Sauter</t><br/>
<t color='#21c3ff'>MAJ + R : </t><t>Menotter (Police)</t><br/>
<t color='#21c3ff'>MAJ + L : </t><t>Gyrophare (Police / Médecin)</t><br/>
<t color='#f2ff21'>F : </t><t>Sirène 1 (Police / Medecin)</t><br/>
<t color='#f2ff21'>CTRL + F : </t><t>Sirène 2 (Police)</t><br/>

<br/>
<t color='#FF0000'>Vous jouez sur la version 2.0 du serveur, n'hésitez pas à nous faire part de toute critique et avis.</t><br/>
;
