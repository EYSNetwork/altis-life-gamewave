#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Prompts the user about disbanding the gang and if the user accepts the gang will be
	disbanded and removed from the database.
*/
private["_action"];

_action = [
	"Vous êtes sur le point de dissoudre votre gang, si vous accepter il sera supprimé de la base de donnée et les membres seront éjecté<br/><br/>Etes vous sur de vouloir dissoudre votre gang ?",
	"Dissoudre gang",
	"Oui",
	"Non"
] call BIS_fnc_guiMessage;

if(_action) then {
	hint "Supression du gang";
	[[grpPlayer],"TON_fnc_removeGang",false,false] spawn life_fnc_MP;
} else {
	hint "Gang supprimé";
};
closedialog 0;