#include <macro.h>
/*
	Author: Bryan "Tonic" Boardwine
	
	Description:
	32 hours later...
*/
private["_grp","_grpMembers"];
if(steamid == (grpPlayer getVariable "gang_owner")) exitWith {hint "You cannot leave the gang without appointing a new leader first!"};


_grp = grpPlayer;
_grpMembers = grpPlayer getVariable "gang_members";
diag_log format["GANGLEAVE CLIENT BEFORE _grpMembers %1",_grpMembers];
_grpMembers = _grpMembers - [steamid];
_grp setVariable["gang_members",_grpMembers,true];
//diag_log format["GANGLEAVE CLIENT AFTER _grpMembers %1",_grpMembers];
//diag_log format["GANGLEAVE CLIENT _grp %1",_grp];
[[4,_grp],"TON_fnc_updateGang",false,false] spawn life_fnc_MP;


closeDialog 0;
hint "Patientez...";
sleep 5;
hint "Vous avez quitté votre Gang.";
[player] joinSilent (createGroup civilian);
life_gangData set [2, ""];