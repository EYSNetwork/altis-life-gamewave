/*
	File: fn_ticketGive.sqf
	Author: Bryan "Tonic" Boardwine
	Traduction : Gamewave
	
	Description:
	Gives a ticket to the targeted player.
*/
private["_val"];
if(isNil {life_dep_ticket_unit}) exitWith {hint "Aucun joueur de séléctionné."};
if(isNull life_dep_ticket_unit) exitWith {hint "Aucun joueur de séléctionné."};
_val = ctrlText 2652;
if(!([_val] call TON_fnc_isnumber)) exitWith {hint "Vous devez rentrer des chiffres pour donner une facture"};
if((parseNumber _val) > 50000) exitWith {hint "Les facture de dépannage peuvent pas dépasser les 50.000€"};
[[0,format["[Activité Civil] Le dépanneur %1 présente une facture de %2€ à %3.",name player,[(parseNumber _val)] call life_fnc_numberText,name life_dep_ticket_unit]],"life_fnc_broadcast",WEST,false] spawn life_fnc_MP;
[[player,(parseNumber _val)],"life_fnc_dep_ticketPrompt",life_dep_ticket_unit,false] spawn life_fnc_MP;
closeDialog 0;