/*
	File: fn_ticketPrompt
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Prompts the player that he is being ticketed.
*/
private["_avocat","_val","_display","_control"];
if(!isNull (findDisplay 2600)) exitwith {}; //Already at the ticket menu, block for abuse?
_avocat = _this select 0;
if(isNull _avocat) exitWith {};
_val = _this select 1;

createDialog "life_avocat_ticket_pay";
disableSerialization;
waitUntil {!isnull (findDisplay 2600)};
_display = findDisplay 2600;
_control = _display displayCtrl 2601;
life_avocat_ticket_paid = false;
life_avocat_ticket_val = _val;
life_ticket_avocat = _avocat;
_control ctrlSetStructuredText parseText format["<t align='center'><t size='.8px'>Facture d'une intervention : %1€",_val];

[] spawn
{
	disableSerialization;
	waitUntil {life_avocat_ticket_paid OR (isNull (findDisplay 2600))};
	if(isNull (findDisplay 2600) && !life_avocat_ticket_paid) then
	{
		//CLIENT
		hint parseText format["<t size='3'><t color='#FF0000'>Facture :</t></t> <br/><t size='1.5'>Vous avez refusé de payé la facture de l'avocat %1, vous êtes automatiquement signalé aux forces de Police pour non-paiement de facture. Si vous pensez être victime d'une arnaque ou d'une erreur rendez-vous directement aux forces de Police.</t>",name life_ticket_avocat];
		systemChat format["Vous avez refusé la facture de votre avocat, vous êtes automatiquement signalé aux force de Police."];
		//COP
		[[0,format["%1 a refusé de payé une facture à l'avocat %2. Consultez la liste de fugitifs.",name player,name life_ticket_avocat]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		//avocatanneur
		[[4,format["<t size='3'><t color='#FF0000'>Facture :</t></t> <br/><t size='1.5'>%1 a refusé de payer votre facture. Un message sera automatiquement envoyé aux forces de Police pour les avertir du non-paiement.</t>",name player]],"life_fnc_broadcast",life_ticket_avocat,false] spawn life_fnc_MP;
		[[0,format["%1 a refusé de payé votre facture. La Police vient d'être contacté pour non-paiement de facture.",name player]],"life_fnc_broadcast",life_ticket_avocat,false] spawn life_fnc_MP;
		
		//ADD WANTEDLIST
		[[getPlayerUID player,name player,"700"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
	};
};