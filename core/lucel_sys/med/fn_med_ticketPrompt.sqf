/*
	File: fn_ticketPrompt
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Prompts the player that he is being ticketed.
*/
private["_med","_val","_display","_control"];
if(!isNull (findDisplay 2600)) exitwith {}; //Already at the ticket menu, block for abuse?
_med = _this select 0;
if(isNull _med) exitWith {};
_val = _this select 1;

createDialog "life_med_ticket_pay";
disableSerialization;
waitUntil {!isnull (findDisplay 2600)};
_display = findDisplay 2600;
_control = _display displayCtrl 2601;
life_med_ticket_paid = false;
life_med_ticket_val = _val;
life_ticket_med = _med;
_control ctrlSetStructuredText parseText format["<t align='center'><t size='.8px'>Honoraire médicale : %1€",_val];

[] spawn
{
	disableSerialization;
	waitUntil {life_med_ticket_paid OR (isNull (findDisplay 2600))};
	if(isNull (findDisplay 2600) && !life_med_ticket_paid) then
	{
		//CLIENT
		hint parseText format["<t size='3'><t color='#FF0000'>Honoraire :</t></t> <br/><t size='1.5'>Vous avez refusé de payé l'honoraire du médecin %1, vous êtes automatiquement signalé aux forces de Police pour non-paiement de facture. Si vous pensez être victime d'une arnaque ou d'une erreur rendez-vous directement aux forces de Police.</t>",name life_ticket_med];
		systemChat format["Vous avez refusé l'honoraire du médecin, vous êtes automatiquement signalé aux force de Police."];
		//COP
		[[0,format["%1 a refusé de payé une honoraire au médecin %2. Consultez la liste de fugitifs.",name player,name life_ticket_med]],"life_fnc_broadcast",west,false] spawn life_fnc_MP;
		//medanneur
		[[4,format["<t size='3'><t color='#FF0000'>Honoraire :</t></t> <br/><t size='1.5'>%1 a refusé de payer votre honoraire. Un message sera automatiquement envoyé aux forces de Police pour les avertir du non-paiement.</t>",name player]],"life_fnc_broadcast",life_ticket_med,false] spawn life_fnc_MP;
		[[0,format["%1 a refusé de payé votre honoraire. La Police vient d'être contacté pour non-paiement d'honoraire.",name player]],"life_fnc_broadcast",life_ticket_med,false] spawn life_fnc_MP;
		
		//ADD WANTEDLIST
		[[getPlayerUID player,name player,"600"],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
	};
};