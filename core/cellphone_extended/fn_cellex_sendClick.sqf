/*

	if send has been clicked
	Edit by altislife.fr
*/
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_index", "_message","_asadmin","_msgtype","_dstdata","_dst"];

_asadmin = _this select 0;

_index = lbCurSel 1500;
_message = ctrlText 1400;

if(_index == -1) exitWith {};
if(_message == "") exitWith {};
if(_asadmin && ((call life_adminlevel) < 1)) exitWith {};

ctrlSetText [1400, ""];
diag_log format["cellex_sendClick | life_smartphoneTarget %1",life_smartphoneTarget];

//Load data
_msgtype = lbValue[1500,_index];
_dstdata = lbData[1500,_index];

if(_msgtype == -1) exitWith {};
//Send message
switch(_msgtype) do 
{
	case 1: //To all police
	{
		if(({side _x == west} count playableUnits) == 0) exitWith {hint format["La police n'est actuellement pas accessible. S'il vous plaît essayer de nouveau plutard..."];};
		//[[_message,name player,1,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
		[[ObjNull,_message,player,1],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		
	};
	case 2: //player => admins
	{
		[[ObjNull,_message,player,2],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,2],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
	};
	case 4: //Admins => All
	{
		[[_message,name player,4],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
		hint "Message envoyé.";
	};
	case 5: //Police => All
	{
		
		[[ObjNull,_message,player,7],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,7],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;
	};

	case 6: //To all Depanneur
	{

		[[ObjNull,_message,player,6],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,6,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;	
	};
	case 7: //Taximesboules
	{
		[[ObjNull,_message,player,9],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,9,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;	
	};
	case 8: //Medicducul
	{
		[[ObjNull,_message,player,10],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,10,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;	
	};
	case 9: //merc
	{
		[[ObjNull,_message,player,11],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,11,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;	
	};
	case 10: //avocat
	{
		[[ObjNull,_message,player,12],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
		hint "Message envoyé.";
		//[[_message,name player,12,player],"TON_fnc_clientMessage",true,false] spawn life_fnc_MP;	
	};
	case 0: //To a person
	{
		life_smartphoneTarget = call compile format["%1",_dstdata];
		if(!("ItemRadio" in assignedItems life_smartphoneTarget)) exitWith {hint "Ce numero n'est pas attribué.";};
		//if(!("ItemRadio" in assignedItems "life_smartphoneTarget")) exitWith {hint "Ce numero n'est pas attribué.";};
		if(isNull life_smartphoneTarget) exitWith {};
		//if(isNil "life_smartphoneTarget") exitWith {};
	
		if(!_asadmin) then //Msgtype 0
		{
			[[life_smartphoneTarget,_message,player,0],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
			hint "Message envoyé.";
			//[[_message,name player,0],"TON_fnc_clientMessage",life_smartphoneTarget,false] spawn life_fnc_MP;
		}
		else //Msgtype 3
		{
			[[life_smartphoneTarget,_message,player,3],"TON_fnc_handleMessages",false] spawn life_fnc_MP;
			hint "Message envoyé.";
			//[[_message,name player,3],"TON_fnc_clientMessage",life_smartphoneTarget,false] spawn life_fnc_MP;
		};
	};
};
