/*
	Opens extended cellphone menu
	LUCELWASHERE
*/
if((player getVariable "restrained")) exitWith {hint "Vous êtes attaché !";closeDialog 0;closeDialog 0;};
if((player getVariable "Surrender")) exitWith {hint "Vous n'avez pas les mains libres !";closeDialog 0;closeDialog 0;};
if(life_is_arrested) exitWith {hint "Pas de téléphone en prison.";closeDialog 0;closeDialog 0;};
if(!("ItemRadio" in assignedItems player)) exitWith {hint "Vous n'avez pas de téléphone portable.";closeDialog 0;closeDialog 0};

private["_display","_units","_type","_index"];

disableSerialization;
waitUntil {!isNull findDisplay 3000};
_display = findDisplay 3000;
lbClear 1500;
ctrlEnable[2403,false];
ctrlEnable[13378,false];
diag_log format["cellex_open | life_smartphoneTarget %1",life_smartphoneTarget];
if !isNull (life_smartphoneTarget) then {
ctrlEnable[13378,true];
};

if((call life_adminlevel) < 1) then
{
	ctrlShow [2402, false];
};

if (("ItemGPS" in (assignedItems player))) then {
ctrlEnable[2403,true];
};

//Fill listbox with default stuff
/////////////////////////////////
_index = lbAdd [1500, "# - Demande Admins/Support -"];
lbSetValue[1500,_index,2];
lbSetColor[1500,_index,[0.8,0,0,1]];

if( (call life_adminlevel) >= 1) then
{
	_index = lbAdd [1500, "# - Message Admin à tous -"];
	lbSetValue[1500,_index,4];
	lbSetColor[1500,_index,[0.8,0,0,1]];
};
_index = lbAdd [1500, "# Police"];
lbSetValue[1500,_index,1];
lbSetColor[1500,_index,[0.08,0.745,0.941,1]];

if( (call life_coplevel) >= 3) then
{
	_index = lbAdd [1500, "# - Message aux Citoyens -"];
	lbSetValue[1500,_index,5];
	lbSetColor[1500,_index,[0.08,0.745,0.941,1]];
};

//Depanneur
_index = lbAdd [1500, "* Dépanneurs"];
lbSetValue[1500,_index,6]; 
lbSetColor[1500,_index,[0.96, 0.52, 0.121, 1]];

//Taxi
_index = lbAdd [1500, "* Taxis"];
lbSetValue[1500,_index,7]; 
lbSetColor[1500,_index,[1, 0.93, 0, 1]];
//Medic
_index = lbAdd [1500, "* Médecins"];
lbSetValue[1500,_index,8]; 
lbSetColor[1500,_index,[0, 1, 0.4, 1]];
//avocats
_index = lbAdd [1500, "* Avocats"];
lbSetValue[1500,_index,10]; 
lbSetColor[1500,_index,[0.50, 0.50, 0, 1]];



//EMPTY PLACEHOLDER
_index = lbAdd [1500, "---------------------------------------------"];
lbSetValue[1500,_index,-1];

//Fill listbox with online players
/////////////////////////////////
{
	if(alive _x && _x != player) then
	{		
		
		switch (side _x) do
		{
			case west: {

				_index = lbAdd [1500, ">"+name _x];
				lbSetData[1500,_index,str(_x)];
				lbSetValue[1500,_index,0];	
				lbSetColor[1500,_index,[0.33,0.62,0.78,1]];
			
			};
			case civilian: {
				_index = lbAdd [1500, name _x];
				lbSetData[1500,_index,str(_x)];
				lbSetValue[1500,_index,0];	
				lbSetColor[1500,_index,[1,1,1,1]];	
				
			
			};
		};		
	};
} foreach playableUnits;
lbsort [_display displayCtrl 1500,"ASC"];

lbSetCurSel [1500,0]; //default