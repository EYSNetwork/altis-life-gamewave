#include <macro.h>
/*
	File: fn_initCop.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Cop Initialization file.
*/
private["_end"];
player addRating 9999998;
waitUntil {!(isNull (findDisplay 46))};
_end = false;
if(life_blacklisted) exitWith
{
	["Blacklisted",false,true] call BIS_fnc_endMission;
	sleep 30;
};

if((str(player) in ["cop_1","cop_2","cop_3","cop_4","cop_5","cop_6","cop_7","cop_8","cop_9","cop_10","cop_11","cop_12","cop_13","cop_14","cop_15","cop_16","cop_17","cop_18","cop_19","cop_20","cop_21","cop_22","cop_23","cop_24","cop_25","cop_26","cop_27","cop_28","cop_29","cop_30","cop_31","cop_32","cop_33","cop_34","cop_35"])) then {
	if((__GETC__(life_coplevel) == 0)) then {
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 25;
	}else{		
	player setVariable["imMedicBro",false,false];
	};
};

//if((__GETC__(life_adminlevel) > 1)) then {
//[] execVM "admintools\loop.sqf";
//[] execVM "admintools\activate.sqf";
//};

if((str(player) in ["cop_32","cop_33"])) then {
	if((__GETC__(life_coplevel) >= 2)) then {
		[] call life_fnc_copmedicLoadout;
	}else{
		["NotWhitelisted",false,true] call BIS_fnc_endMission;
		sleep 25;
	};
};

// Set Paycheck for Police
switch (__GETC__(life_coplevel)) do 
			{
				case 1: { life_paye = ((1000)); }; //Cadet
				case 2: { life_paye = ((3000)); }; //Officer
				case 3: { life_paye = ((4000)); }; //Cpl
				case 4: { life_paye = ((5000)); }; //Sgt
				case 5: { life_paye = ((6000)); }; //Lt
				case 6: { life_paye = ((6500)); }; //Captain
				default { life_paye = ((10000)); }; //default in-case anything goes tits up
			};

if (life_alive) then
{
	player setPos [life_bidet select 0, life_bidet select 1, (life_bidet select 2) +1];

}
else
{
	
	[] call life_fnc_spawnMenu;
	waitUntil{!isNull (findDisplay 38500)}; //Wait for the spawn selection to be open.
	waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.
};




	
[] call life_fnc_updateClothing;