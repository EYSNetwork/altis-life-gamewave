/*
	File: fn_onPlayerRespawn.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Does something but I won't know till I write it...
*/
private["_unit","_corpse"];
_unit = _this select 0;
_corpse = _this select 1;
life_corpse = _corpse;

//life_respawned = true;

//Set some vars on our new body.
_unit setVariable["restrained",FALSE,TRUE];
_unit setVariable["CopRestrain",FALSE,TRUE];
_unit setVariable["Escorting",FALSE,TRUE];
_unit setVariable["transporting",FALSE,TRUE]; //Again why the fuck am I setting this? Can anyone tell me?
_unit setVariable["LetalDamageLucel",TRUE,TRUE];
//_unit setVariable ["FAR_isUnconscious", 0, true];
//_unit setVariable ["FAR_isDragged", 0, true];


//Load our gear as a cop incase something horrible happens
if(playerSide == west) then {
	[] spawn life_fnc_loadGear;
};

_unit addRating 9999999999999999; //Set our rating to a high value, this is for a ARMA engine thing.
player playMoveNow "amovppnemstpsraswrfldnon";

[] call life_fnc_setupActions;
//[] spawn FAR_Player_Init;
//[] call life_fnc_equipGear;
[] call life_fnc_updateClothing;
[[_unit,life_sidechat,playerSide],"TON_fnc_managesc",false,false] spawn life_fnc_MP;