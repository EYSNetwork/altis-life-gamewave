
/*
	File: fn_pricesUpdate
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Sends the update request to the server to save information in the database.
*/


private["_items","_montant","_amoundadd","_prix","_var","_packet"];
_items = [_this,0,"",[""]] call BIS_fnc_param; //The item we are using to add or remove.
_montant = [_this,1,0,[0]] call BIS_fnc_param; //Number of items to add or remove.
_prix = [_this,2,0,[0]] call BIS_fnc_param; //Number of items to add or remove.
_var = [_this,3,"",[""]] call BIS_fnc_param; //The item we are using to add or remove.
//_items = this select 0;
//_montant = this select 1;
//_prix = this select 2;
diag_log format ["Prices update client Namitems %1 | _var %2 | _amount %3 | _price %4", _items, _var, _montant, _prix];
_packet = [_items,_montant,_prix,_var];


[_packet,"DB_fnc_prices_updateRequest",false,false] spawn life_fnc_MP;


/*
	File:

private["_packet","_array","_flag"];
_packet = [getPlayerUID player,name player,playerSide,life_flouze,life_dabflouze];
_array = [];
_flag = switch(playerSide) do {case west: {"cop"}; case civilian: {"civ"}; case independent: {"med"};};
{
	if(_x select 1 == _flag) then
	{
		_array set[count _array,[_x select 0,(missionNamespace getVariable (_x select 0))]];
	};
} foreach life_licenses;

_packet set[count _packet,_array];
switch (playerSide) do {
	case west: {_packet set[count _packet,cop_gear];
			_packet set[9, getPos player];
			_packet set[10, alive player];
		};
	case civilian: {
		[] call life_fnc_civFetchGear;
		_packet set[count _packet,civ_gear];
		_packet set[count _packet,life_is_arrested];
		_packet set[9, getPos player];
		_packet set[10, alive player];
	};
};

[_packet,"DB_fnc_updateRequest",false,false] spawn life_fnc_MP;
*/