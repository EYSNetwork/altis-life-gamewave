/*
File: fn_RepaintVehicle.sqf
Author: MooN-Gaming

Description: Does the active repaint man....read the title!
*/
private["_index","_veh","_color","_color_index"];
if(!isNull (findDisplay 2300)) then {
_veh = nearestObject [position player, "LandVehicle"];
_color = lbcursel 2303;
_color_index = lbValue[2303,_color];
closeDialog 0;

_baseprice = 10000;
_vehicleData = _veh getVariable["vehicle_info_owners",[]];
_vehOwner = (_vehicleData select 0) select 0;
if(life_flouze < _basePrice) exitWith {hint parseText format["<t size='3'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1.5'>Vous n'avez pas 10.000€ sur vous !</t>"];};

if(isNil {_vehicleData}) exitWith {hint parseText format["<t size='3'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1.5'>Ce véhicule n'existe pas, l'avez vous cheat ?</t>"];};
if ((getPlayerUID player) != _vehOwner) exitWith {hint "You are not the owner of the vehicle that is to be painted."};

		
		_displayName = getText(configFile >> "CfgVehicles" >> (typeOf _veh) >> "displayName");
		_upp = format["Painting %1",_displayName];
		
		//Setup our progress bar.
		disableSerialization;
		5 cutRsc ["life_progress","PLAIN"];
		_ui = uiNameSpace getVariable "life_progress";
		_progress = _ui displayCtrl 38201;
		_pgText = _ui displayCtrl 38202;
		_pgText ctrlSetText format["%2 (1%1)...","%",_upp];
		_progress progressSetPosition 0.01;
		_cP = 0.01;
		
		life_action_inUse = true;  //<---------MOVE THIS TO HERE
		
		while{true} do
		{
			if(animationState player != "AinvPknlMstpSnonWnonDnon_medic_1") then {
				[[player,"AinvPknlMstpSnonWnonDnon_medic_1"],"life_fnc_animSync",true,false] spawn life_fnc_MP;
				player playMoveNow "AinvPknlMstpSnonWnonDnon_medic_1";
			};						
			sleep 0.29;
			//[player,"spraycan"] call life_fnc_globalSound; //Globaler Sound

			_cP = _cP + 0.01;
			_progress progressSetPosition _cP;
			_pgText ctrlSetText format["%3 (%1%2)...",round(_cP * 100),"%",_upp];			
			if(_cP >= 1) exitWith {};
			if(!alive player) exitWith {};
			if(player != vehicle player) exitWith {};
			if(life_interrupted) exitWith {};
		};
		
		life_action_inUse = false;
		
		5 cutText ["","PLAIN"];
		player playActionNow "stop";
		if(life_interrupted) exitWith {life_interrupted = false; titleText["Annulé !","PLAIN"]; life_action_inUse = false;};
		if(player != vehicle player) exitWith {hint parseText format["<t size='3'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1.5'>Vous devez être hors de votre véhicule pour le repeindre !</t>"];};
	
		life_flouze = life_flouze - _basePrice;
		//Send toDB
		[[_veh,_color_index],"TON_fnc_vehicleRepaint",false,false] spawn life_fnc_MP;
		
		//Color vehicle locally
		[_veh,_color_index] call life_fnc_colorVehicle;
		
		[] call SOCK_fnc_updateRequest; //Sync silently because it's obviously silently..
	
		hint parseText format["<t size='2'><t color='#008000'>Véhicule repeint !</t></t> <br/><t size='1.5'>Vous pouvez venir repeindre votre véhicule quand vous voulez !</t>"];
};