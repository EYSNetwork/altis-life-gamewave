/*

	Updates player clothing by replacing vanilla by custom ones
        by Audacious

*/

private["_uniform","_backpack","_idgrp"];

///Uniform
////////////////
_idgrp = group player;
_uniform = uniform player;

switch(true) do
{
	//COP
	case (_uniform == "U_Rangemaster" && !license_civ_medic && playerSide == West):
	{
		player setObjectTextureGlobal  [0, "texture\skins\cop\Uniforme_recrue.jpg"]; 
	};
	case (_uniform == "U_B_CTRG_1"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\cop\medic_police.paa"]; 
	};
	case (_uniform == "U_B_CombatUniform_mcam_vest"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\cop\aor2.paa"];
	};	
		
	case (_uniform == "U_B_CombatUniform_mcam_tshirt"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\cop\aor2.paa"];
	};		

	case (_uniform == "U_B_survival_uniform"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\cop\aor2.paa"];
	};	
	//SKINMERC
	case (_uniform == "U_O_CombatUniform_oucamo"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\merc\U_PMC_CombatUniform_ChckDBS_GPSB.paa"]; 
	};
	case (_uniform == "U_B_CombatUniform_mcam_worn"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\merc\U_PMC_CombatUniform_GSBPBB.paa"]; 
	};	
	
	//CIVIL
	case (_uniform == "U_C_Scientist"):
	{
		player setObjectTextureGlobal  [0, "texture\skins\medic\medecin.paa"]; 
	};		
	
	//BlackFlag
	case (_uniform == "U_O_SpecopsUniform_ocamo" && (_idgrp getVariable "gang_id") == (560)):
	{
		player setObjectTextureGlobal  [0, "texture\skins\reb\reb_blackflag.paa"];
	};	
	
	//Nostra
	case (_uniform == "U_I_CombatUniform" && (_idgrp getVariable "gang_id") == (577)):
	{
		player setObjectTextureGlobal  [0, "texture\skins\reb\reb_nostra.paa"];
	};	

	
	
};

///backpack
////////////////
