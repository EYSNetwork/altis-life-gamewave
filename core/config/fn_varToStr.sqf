/*
	File: fn_varToStr.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Takes the long-name (variable) and returns a display name for our
	virtual item.
*/
private["_var"];
_var = [_this,0,"",[""]] call BIS_fnc_param;
if(_var == "") exitWith {""};

switch (_var) do
{
	//Virtual Inventory Items
	case "life_inv_oilu": {"Pétrole brut"};
	case "life_inv_oilp": {"Sans Plomb 95"};
	case "life_inv_houblon": {"Houblon"};
	case "life_inv_houblonp": {"Bière d'Abbaye"};
	case "life_inv_heroinu": {"Pavot"};
	case "life_inv_heroinp": {"Héroine"};
	case "life_inv_cannabis": {"Cannabis"};
	case "life_inv_marijuana": {"Marijuana"};
	case "life_inv_apple": {"Pomme"};
	case "life_inv_rabbit": {"Viande de lapin"};
	case "life_inv_salema": {"Filet de Saumon"};
	case "life_inv_ornate": {"Filet de Némo"};
	case "life_inv_mackerel": {"Maquereau à la moutarde"};
	case "life_inv_tuna": {"Gros thon"};
	case "life_inv_mullet": {"Escalope de Dauphin"};
	case "life_inv_catshark": {"Pyrana farcie"};
	case "life_inv_turtle": {"Viande de tortue"};
	case "life_inv_fishingpoles": {"Canne à pêche"};
	case "life_inv_water": {"Bouteille d'eau"};
	case "life_inv_coffee": {"Café"};
	case "life_inv_turtlesoup": {"Soupe de Tortue"};
	case "life_inv_donuts": {"Donuts"};
	case "life_inv_fuelE": {"Jerricane vide"};
	case "life_inv_fuelF": {"Jerricane d'essence"};
	case "life_inv_pickaxe": {"Pioche"};
	case "life_inv_copperore": {"Cuivre"};
	case "life_inv_ironore": {"Plaque de Fer"};
	case "life_inv_ironr": {"Lingot de Fer"};
	case "life_inv_copperr": {"Lingot de Cuivre"};
	case "life_inv_sand": {"Sable"};
	case "life_inv_salt": {"Sel"};
	case "life_inv_saltr": {"Sel raffiné"};
	case "life_inv_glass": {"Vitre"};
	case "life_inv_diamond": {"Diamant brut"};
	case "life_inv_diamondr": {"Diamant taillé"};
	case "life_inv_tbacon": {"Kebab"};
	case "life_inv_redgull": {"RedBull"};
	case "life_inv_lockpick": {"Lockpick"};
	case "life_inv_peach": {"Pêche"};
	case "life_inv_coke": {"Cocaïne"};
	case "life_inv_cokep": {"Cocaïne Traitée"};
	case "life_inv_spikeStrip": {"Herse"};
	case "life_inv_rock": {"Rocher"};
	case "life_inv_cement": {"Sac de Ciment"};
	case "life_inv_goldbar": {"Lingot d'Or"};
	case "life_inv_menotte": {"Paire de Menottes"};
	case "life_inv_storage1": {"Petit coffre"};
    case "life_inv_storage2": {"Gros coffre"};
    case "life_inv_meth": {"Méthamphétamine"};
    case "life_inv_phos": {"Phosphore Rouge"};
    case "life_inv_soude": {"Soude Caustique"};
	case "life_inv_storagesmall": {"Petit coffre"};
	case "life_inv_storagebig": {"Grand coffre"};
	case "life_inv_mauer": {"Barrière"};
	case "life_inv_trackerlucel": {"Traceur"};

	case "life_inv_blastingcharge": {("C4")};
	case "life_inv_boltcutter": {("Disqueuse")};
	case "life_inv_defusekit": {("Kit de désamorçage")};
	case "life_inv_scannerlucel": {("Détecteur d'explosif")};
	
	//License Block
	case "license_civ_driver": {"Permis de conduire"};
	case "license_civ_air": {"License de pilote d'hélicoptère"};
	case "license_civ_heroin": {"Traitement de l'heroine"};
	case "license_civ_gang": {"License de gang"};
	case "license_civ_oil": {"Traitement du pétrole"};
	case "license_civ_houblon": {"Brassage de la bière"};
	case "license_civ_dive": {"License de plongée"};
	case "license_civ_boat": {"Permis bateaux"};
	case "license_civ_gun": {"Permis port d'armes légale"};
	case "license_cop_air": {"License de pilote d'hélicoptère"};
	case "license_cop_swat": {"Entrainement SWAT"};
	case "license_cop_cg": {"Gardien de la paix"};
	case "license_civ_rebel": {"Entrainement rebelle"};
	case "license_civ_truck": {"Permis poids lourd"};
	case "license_civ_diamond": {"Traitement du diamant"};
	case "license_civ_copper": {"Traitement du cuivre"};
	case "license_civ_iron": {"Traitement de fer"};
	case "license_civ_sand": {"Traitement de sable"};
	case "license_civ_salt": {"Traitement de sel"};
	case "license_civ_coke": {"Traitement de cocaïne"};
	case "license_civ_marijuana": {"Traitement de cannabis"};
	case "license_civ_cement": {"Traitement du ciment"};
	case "license_civ_dep": {"License de dépanneur"};
	case "license_civ_home": {"Droit de propriété"};	
	case "license_civ_meth": {"Préparation méthamphétamine"};	
	case "license_civ_taxi": {"License de taxi"};	
	case "license_civ_medic": {"License de médecin"};	
	case "license_civ_merc": {"License de mercenaire"};
	case "license_civ_avocat": {"License d'avocat"};	
	case "license_civ_pref": {"Mandat de prefet"};	
};
