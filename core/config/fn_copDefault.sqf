/*
	File: fn_copDefault.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Default cop configuration.
*/
//Strip the player down
RemoveAllWeapons player;
{player removeMagazine _x;} foreach (magazines player);
removeUniform player;
removeVest player;
removeBackpack player;
removeGoggles player;
removeHeadGear player;
{
	player unassignItem _x;
	player removeItem _x;
} foreach (assignedItems player);

//Load player with default cop gear.
player addUniform "U_Rangemaster";
player addVest "V_TacVest_blk_POLICE";
player addHeadgear "H_Cap_police";

player addWeapon "hgun_Pistol_heavy_02_Yorris_F";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";
player addMagazine "6Rnd_45ACP_Cylinder";

/* ITEMS */
player addItem "ItemMap";
player assignItem "ItemMap";
player addItem "ItemCompass";
player assignItem "ItemCompass";
player addItem "ItemWatch";
player assignItem "ItemWatch";
player addItem "ItemGPS";
player assignItem "ItemGPS";

[] call life_fnc_saveGear;