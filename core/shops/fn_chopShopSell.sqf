/*
	File: fn_chopShopSell.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Sells the selected vehicle off.
*/
disableSerialization;
private["_control","_price","_vehicle","_nearVehicles","_price2","_check"];
_control = ((findDisplay 39400) displayCtrl 39402);
_price = _control lbValue (lbCurSel _control);
_vehicle = _control lbData (lbCurSel _control);
_vehicle = call compile format["%1", _vehicle];
diag_log format ["ChopShop _vehicle %1", _vehicle];
_nearVehicles = nearestObjects [getMarkerPos life_chopShop,["Car","Truck","Air"],25];



_vehicle = _nearVehicles select _vehicle;
if(isNull _vehicle) exitWith {};
//_nearVehicles resize 1;
_check = _nearVehicles select 0;
//diag_log format ["ChopShop _control %1 | _price %2 | _vehicle %3 | _nearvehicles %4 | _check %5", _control, _price, _vehicle, _nearVehicles, _check];
//diag_log format ["ChopShop _nearPlayers %1 | _nearVehicles %2 ", count _nearPlayers, count _nearVehicles];
//if((TypeOF _vehicle) == (TypeOF _check)) then {

if ((count _nearVehicles > 1)) then {
hint "Attend il y'a plusieurs véhicules dans la zone, j'arrive pas à me concentrer, éloigne en quelques uns !";
closeDialog 0;
}
else
{
hint "Vente du véhicule...";
life_action_inUse = true;
_price2 = life_flouze + _price;
[[player,_vehicle,_price,_price2],"TON_fnc_chopShopSell",false,false] spawn life_fnc_MP;
closeDialog 0;
};