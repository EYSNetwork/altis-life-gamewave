/*
	File: fn_chopShopMenu.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Opens & initializes the chop shop menu.
*/
if(life_action_inUse) exitWith {hint "Attendez 2 minutes svp..."};
disableSerialization;
private["_nearVehicles","_control","_nearPlayers","_playersList"];
_nearVehicles = nearestObjects [getMarkerPos (_this select 3),["Car","Truck","Air"],25];
_nearPlayers = nearestObjects [getMarkerPos (_this select 3),["Man"],25];
_playerslist =[];

{
	if(_x in playableUnits) then {
	_playerslist pushBack _x;
	};
} foreach _nearPlayers;

life_chopShop = (_this select 3);

//diag_log format ["ChopShop _playerslist %1 | _playerslist %2 ", count _playerslist,_playerslist];
//diag_log format ["ChopShop _nearPlayers %1 | _nearPlayers %2 ", count _nearPlayers,_nearPlayers];
//diag_log format ["ChopShop _nearVehicles %1 | _nearVehicles %2 ", count _nearVehicles,_nearVehicles];
//Error check
if(count _nearVehicles == 0) exitWith {titleText["Aucun véhicule a proximité.","PLAIN"];};

if(count _playerslist > 1) exitWith {titleText["Trop de joueurs a proximité","PLAIN"];};

if(!(createDialog "Chop_Shop")) exitWith {hint "Ah... ça bug..."};

_control = ((findDisplay 39400) displayCtrl 39402);
{
	if(alive _x) then {
		_className = typeOf _x;
		_displayName = getText(configFile >> "CfgVehicles" >> _className >> "displayName");
		_picture = getText(configFile >> "CfgVehicles" >> _className >> "picture");
		_ind = [_className,(call life_garage_sell)] call TON_fnc_index;
		
		if(_ind != -1 && count crew _x == 0) then {
			_price = ((call life_garage_sell) select _ind) select 1;
			_control lbAdd _displayName;
			_control lbSetData [(lbSize _control)-1,str(_forEachIndex)];
			_control lbSetPicture [(lbSize _control)-1,_picture];
			_control lbSetValue [(lbSize _control)-1,_price];
		};
	};
} foreach _nearVehicles;