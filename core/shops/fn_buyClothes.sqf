/*
	File: fn_buyClothes.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Buys the current set of clothes and closes out of the shop interface.
*/
private["_price"];
if((lbCurSel 3101) == -1) exitWith {titleText["Vous n'avez pas choisis ce que vous voulez acheter !","PLAIN"];};

_price = 0;
{
	if(_x != -1) then
	{
		_price = _price + _x;
	};
} foreach life_clothing_purchase;

if(_price > life_flouze) exitWith {titleText["Désolé mais tu n'a pas assez d'argent pour acheter ces fringues.","PLAIN"];};
life_flouze = life_flouze - _price;

life_clothesPurchased = true;
closeDialog 0;
//[] call life_fnc_equipGear;
[] call life_fnc_updateClothing;