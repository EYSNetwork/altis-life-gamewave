#include <macro.h>
/*
	fn_vehicleAssur.sqf
	think & tweak by lecul / altislife.fr
*/


if((time - life_action_delay) < 10) exitWith {hint parseText format["<t size='2'><t color='#f2ff20'>Attendez 10 secondes</t></t> <br/><t size='1'>avant d'assurer à nouveau  !</t>"];  };
life_action_delay = time;
private["_vehicle","_vid","_pid","_unit","_price"];
disableSerialization;
if(lbCurSel 2802 == -1) exitWith {hint localize "STR_Global_NoSelection"};
_vehicle = lbData[2802,(lbCurSel 2802)];
_vehicle = (call compile format["%1",_vehicle]) select 0;
_vid = lbValue[2802,(lbCurSel 2802)];
_pid = getPlayerUID player;
_unit = player;


if(isNil "_vehicle") exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Il y a une erreur dans la séléction !</t>"];  };

_price = [_vehicle,__GETC__(life_assur_prices)] call TON_fnc_index;
if(_price == -1) then {_price = 10000;} else {_price = (__GETC__(life_assur_prices) select _price) select 1;};
if(life_dabflouze < _price) exitWith {hint format[(localize "STR_Garage_CashError"),[_price] call life_fnc_numberText];};

[[_vid,_pid,(getMarkerPos life_garage_sp),_unit,_price],"TON_fnc_vehicleAssur",false,false] spawn life_fnc_MP;

hint parseText format["<t size='2'><t color='#008000'>Assurance :</t></t> <br/><t size='1'>Vôtre véhicule est désormais assuré !</t>"]; 

life_dabflouze = life_dabflouze - _price;
closeDialog 0;
