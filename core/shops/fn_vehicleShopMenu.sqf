/*
	File: fn_vehicleShopMenu.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Blah
	LUCEL
*/
private["_shop","_sideCheck","_spawnPoints","_shopFlag","_disableBuy"];
_shop = [(_this select 3),0,"",[""]] call BIS_fnc_param;
_sideCheck = [(_this select 3),1,sideUnknown,[civilian]] call BIS_fnc_param;
_spawnPoints = [(_this select 3),2,"",["",[]]] call BIS_fnc_param;
_shopFlag = [(_this select 3),3,"",[""]] call BIS_fnc_param;
_disableBuy = [(_this select 3),5,false,[true]] call BIS_fnc_param;

diag_log format ["vehicleshopBuy-- _shop %1 | _sideCheck %2 | _spawnPoints %3 | _shopFlag %4 | _disableBuy %5 ", _shop, _sideCheck, _spawnPoints, _shopFlag, _disableBuy];

disableSerialization;
//Long boring series of checks
if(dialog) exitWith {};
if(_shop == "") exitWith {};
if(_sideCheck != sideUnknown && {playerSide != _sideCheck}) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous n'êtes pas autorisé à utiliser ce magasin !</t>"];};

if((_shop == "med_air_hs" && !license_civ_air)) exitWith {hint parseText format["<t size='2'><t color='#00FF00'>Permis :</t></t> <br/><t size='1'>Vous avez besoin d'une license de pilote.</t>"]};
if((_shop == "dep_air" && !license_civ_air)) exitWith {hint parseText format["<t size='2'><t color='#00FF00'>Permis :</t></t> <br/><t size='1'>Vous avez besoin d'une license de pilote.</t>"]};
if((_shop == "merc_air" && !license_civ_air)) exitWith {hint parseText format["<t size='2'><t color='#00FF00'>Permis :</t></t> <br/><t size='1'>Vous avez besoin d'une license de pilote.</t>"]};



if(!createDialog "Life_Vehicle_Shop_v2") exitWith {};

life_veh_shop = [_shop,_spawnpoints,_shopFlag,_disableBuy]; //Store it so so other parts of the system can access it.

ctrlSetText [2301,((_this select 3) select 4)];

if(_disableBuy) then {
	//Disable the buy button.
	ctrlEnable [2309,false];
};

//Fetch the shop config.
_vehicleList = [_shop] call life_fnc_vehicleListCfg;

_control = ((findDisplay 2300) displayCtrl 2302);
lbClear _control; //Flush the list.
ctrlShow [2330,false];
ctrlShow [2304,false];

//Boucle de la liste des vehicules
{
	_className = _x select 0;
	_basePrice = _x select 1;	
	_vehicleInfo = [_className] call life_fnc_fetchVehInfo;
// MEDIC SHOP
if (_shopFlag == "medic") then {
	if((_className == "C_SUV_01_F")) then {
		_control lbAdd format["SUV Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};
	if((_className == "C_Hatchback_01_F")) then {
		_control lbAdd format["Hayon Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};
	if((_className == "C_Hatchback_01_sport_F")) then {
		_control lbAdd format["Hayon Sport Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};
	if((_className == "B_Heli_Light_01_F")) then {
		_control lbAdd format["MH-9 Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};		
	if((_className == "O_Heli_Light_02_unarmed_F")) then {
		_control lbAdd format["Orca Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};	
	if((_className == "O_Heli_Transport_04_medevac_F")) then {
		_control lbAdd format["Taru Secouriste"];
		_control lbSetColor [(lbSize _control)-1,[0, 1, 0.4, 1]];
		};
	};
// TAXI SHOP
if (_shopFlag == "taxi")then{
	if((_className == "C_Offroad_01_F")) then {
		_control lbAdd format["Pick-Up Taxi"];
		_control lbSetColor [(lbSize _control)-1,[1, 0.93, 0, 1]];
		};
	if((_className == "C_SUV_01_F")) then {
		_control lbAdd format["SUV Taxi"];
		_control lbSetColor [(lbSize _control)-1,[1, 0.93, 0, 1]];
		};
	};
// DEP SHOP
if (_shopFlag == "dep")then{
	if((_className == "C_Offroad_01_F")) then {
	_control lbAdd format["Pick-Up Dépanneur"];
	_control lbSetColor [(lbSize _control)-1,[0.94, 0.37, 0.13, 1]];
	};
	if((_className == "O_Heli_Transport_04_repair_F")) then {
	_control lbAdd format["Taru Dépanneur"];
	_control lbSetColor [(lbSize _control)-1,[0.94, 0.37, 0.13, 1]];
	};
	};
		
if (_shopFlag == "civ" || _shopFlag == "reb" || _shopFlag == "cop" || _shopFlag == "merc") then {
		_control lbAdd (_vehicleInfo select 3);
		_control lbSetColor [(lbSize _control)-1,[0.24, 0.41, 0.15, 1]];
};
	
	_control lbSetPicture [(lbSize _control)-1,(_vehicleInfo select 2)];
	_control lbSetData [(lbSize _control)-1,_className];
	_control lbSetValue [(lbSize _control)-1,_ForEachIndex];

} foreach _vehicleList;