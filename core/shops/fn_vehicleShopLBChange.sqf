#include <macro.h>
/*
	File: fn_vehicleShopLBChange.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Called when a new selection is made in the list box and
	displays various bits of information about the vehicle.
*/
disableSerialization;
private["_control","_index","_className","_basePrice","_vehicleInfo","_colorArray","_ctrl"];
_control = _this select 0;
_index = _this select 1;

//Fetch some information.
_className = _control lbData _index;
_vIndex = _control lbValue _index;
_vehicleList = [life_veh_shop select 0] call life_fnc_vehicleListCfg; _basePrice = (_vehicleList select _vIndex) select 1;
_vehicleInfo = [_className] call life_fnc_fetchVehInfo;
_trunkSpace = [_className] call life_fnc_vehicleWeightCfg;
//diag_log format ["vehicleshopLBchange-- _className %1, _vIndex %2, _vehicleList %3, _vehicleInfo %4, _trunkSpace %5 ----", _className, _vIndex, _vehicleList, _vehicleInfo, _trunkSpace];

ctrlShow [2330,true];
(getControl(2300,2303)) ctrlSetStructuredText parseText format[
"Prix d'achat: <t color='#8cff9b'>$%2</t><br/>
Vitesse maximale: %3 km/h<br/>Puissance du moteur: %4<br/>
Nombre de places: %5<br/>
Capacité du coffre: %6<br/>
Capacité réservoir: %7<br/>
Résistance : %8",
[_basePrice] call life_fnc_numberText,
[round(_basePrice * 1.5)] call life_fnc_numberText,
_vehicleInfo select 8,
_vehicleInfo select 11,
_vehicleInfo select 10,
if(_trunkSpace == -1) then {"None"} else {_trunkSpace},
_vehicleInfo select 12,
_vehicleInfo select 9
];
_ctrl = getControl(2300,2304);
lbClear _ctrl; //reset
_colorArray = [_className] call life_fnc_vehicleColorCfg;
for "_i" from 0 to count(_colorArray)-1 do {
if((_colorArray select _i) select 1 == (life_veh_shop select 2)) then {
_temp = [_className,_i] call life_fnc_vehicleColorStr;
_ctrl lbAdd format["%1",_temp];
_ctrl lbSetValue [(lbSize _ctrl)-1,_i];
//diag_log format ["vehicleshopLBchange-- _temp %1",_temp];
//diag_log format ["vehicleshopLBchange-- _colorArray %1",_colorArray];
};
};
lbSetCurSel[2304,0];
if((lbSize _ctrl)-1 != -1) then {
	ctrlShow[2304,true];
} else {
	ctrlShow[2304,false];
};

/*

if(_className in (__GETC__(life_vShop_rentalOnly))) then {
	ctrlEnable [2309,false];
} else {
	if(!(life_veh_shop select 3)) then {
		ctrlEnable [2309,true];
	};
};
*/

true;