/*
	File: fn_giveMoney.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Gives the selected amount of money to the selected player.
*/
if((time - life_action_delay) < 5) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_unit","_amount","_idGive","_idReceive"];
_amount = ctrlText 2018;
ctrlShow[2001,false];
if((lbCurSel 2022) == -1) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous devez séléctionner un joueur !</t>"];ctrlShow[2001,true];};
_unit = lbData [2022,lbCurSel 2022];
_unit = call compile format["%1",_unit];
if(isNil "_unit") exitWith {ctrlShow[2001,true];};
if(_unit == player) exitWith {ctrlShow[2001,true];};
if(isNull _unit) exitWith {ctrlShow[2001,true];};
if(player distance _unit > 10 ) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous devez séléctionner un joueur !</t>"];ctrlShow[2001,true];}; //Havena ajout
_idGive = getPlayerUID player;
_idReceive = getPlayerUID _unit;

//A series of checks *ugh*
if(!life_use_atm) exitWith {hint "Vous avez récement volé la banque ! Vous ne pouvez pas donner de l'argent comme ça.";ctrlShow[2001,true];};
if(!([_amount] call TON_fnc_isnumber)) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Merci d'indiquer un nombre valide !</t>"];ctrlShow[2001,true];};
if(parseNumber(_amount) <= 0) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Merci d'indiquer un nombre valide !</t>"];ctrlShow[2001,true];};
if(parseNumber(_amount) > life_flouze) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous n'avez pas assez d'argent sur vous !</t>"];ctrlShow[2001,true];};
if(isNull _unit) exitWith {ctrlShow[2001,true];};
if(isNil "_unit") exitWith {ctrlShow[2001,true]; hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Le joueur doit se trouver à proximité !</t>"];  };
hint parseText format["<t size='2'><t color='#008000'>Transaction réussie :</t></t> <br/><t size='1'>Vous avez envoyé </t><t color='#f2ff20'>%1€</t> !",[(parseNumber(_amount))] call life_fnc_numberText,name _unit];
life_flouze = life_flouze - (parseNumber(_amount));

//[[getPlayerUID player,playerSide,life_flouze,6,life_dabflouze],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;
[6] call SOCK_fnc_updatePartial; // Sync du life_flouze
//[] call SOCK_fnc_updateRequest;

[[_unit,_amount,player,_idGive,_idReceive],"life_fnc_receiveMoney",_unit,false] spawn life_fnc_MP;
[] call life_fnc_p_updateMenu;

ctrlShow[2001,true];