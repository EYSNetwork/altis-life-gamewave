/*
	File: fn_giveItem.sqf
	Author: Bryan "Tonic" Boardwine
	Traduction : Gamewave
	
	Description:
	Gives the selected item & amount to the selected player and
	removes the item & amount of it from the players virtual
	inventory.
*/
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_unit","_val"];
_val = ctrlText 2010;
ctrlShow[2002,false];
if((lbCurSel 2023) == -1) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Vous devez séléctionner un joueur !</t>"];ctrlShow[2002,true];};
_unit = lbData [2023,lbCurSel 2023];
_unit = call compile format["%1",_unit];
if((lbCurSel 2005) == -1) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Choisissez un objet !</t>"];ctrlShow[2002,true];};
_item = lbData [2005,(lbCurSel 2005)];
if(isNil "_unit") exitWith {ctrlShow[2002,true];};
if(_unit == player) exitWith {ctrlShow[2002,true];};
if(isNull _unit) exitWith {ctrlShow[2002,true];};

//A series of checks *ugh*
if(!([_val] call TON_fnc_isnumber)) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Merci d'indiquer un nombre valide !</t>"];ctrlShow[2002,true];};
if(parseNumber(_val) <= 0) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Merci d'indiquer un nombre valide !</t>"];ctrlShow[2002,true];};
if(isNil "_unit") exitWith {ctrlShow[2001,true]; hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Le joueur doit se trouver à proximité !</t>"];  };
if(!([false,_item,(parseNumber _val)] call life_fnc_handleInv)) exitWith {hint parseText format["<t size='2'><t color='#ff0000'>Erreur :</t></t> <br/><t size='1'>Tu ne peux pas donner autant !</t>"];ctrlShow[2002,true];};
[[_unit,_val,_item,player],"life_fnc_receiveItem",_unit,false] spawn life_fnc_MP;
_type = [_item,0] call life_fnc_varHandle;
_type = [_type] call life_fnc_varToStr;
hint parseText format["<t size='2'><t color='#008000'>Envoi réussi :</t></t> <br/><t size='1'>Vous avez donné </t><t color='#f2ff20'>%2 %3</t> !",name _unit,_val,_type];
[] call life_fnc_p_updateMenu;

ctrlShow[2002,true];