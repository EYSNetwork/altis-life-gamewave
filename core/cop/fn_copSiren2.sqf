/*
	File: fn_copSiren2.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Starts the cop siren sound for other players
*/
private["_vehicle"];
_vehicle = [_this,0,ObjNull,[ObjNull]] call BIS_fnc_param;

if(isNull _vehicle) exitWith {};
if(isNil {_vehicle getVariable "Yelp"}) exitWith {};

while {true} do
{
	if(!(_vehicle getVariable "Yelp")) exitWith {};
	if(count (crew (_vehicle)) == 0) then {_vehicle setVariable["Yelp",false,true]};
	if(!alive _vehicle) exitWith {};
	if(isNull _vehicle) exitWith {};
	_vehicle say3D "Yelp";
	sleep 4.7;
	if(!(_vehicle getVariable "Yelp")) exitWith {};
};