/*
	File: fn_bountyReceive.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Notifies the player he has received a bounty and gives him the cash.
*/
private["_val","_total"];
_val = [_this,0,"",["",0]] call BIS_fnc_param;
_total = [_this,1,"",["",0]] call BIS_fnc_param;

if(_val == _total) then
{
	titleText[format["Tu as gagné %1€ pour avoir mis en prison un criminel, bon boulot !",[_val] call life_fnc_numberText],"PLAIN"];
}
	else
{
	titleText[format["Tu as gagné %1€ pour avoir tuer un criminel, mais si tu l'avais mis en prison tu aurais gagner %2€",[_val] call life_fnc_numberText,[_total] call life_fnc_numberText],"PLAIN"];
};

life_dabflouze = life_dabflouze + _val;