/*
        File: fn_questionDealer.sqf
        Author: Bryan "Tonic" Boardwine
       
        Description:
        Questions the drug dealer and sets the sellers wanted.
*/
private["_sellers","_names"];
_sellers = (_this select 0) getVariable["sellers",[]];
if (life_flouze<1500)  exitWith  {hint "Il me faut un plus gros billet mec ! "};
if (life_flouze >=1500)  then
{
   life_flouze = life_flouze - 1500;
   if(count _sellers == 0) exitWith {hint "J'suis en rade, personne ne m'as rien vendu ! J'te le jure mec !"}; //No data.
   life_action_inUse = true;
   _names = "";
  {
          if(_x select 2 > 150000) then
          {
                   _val = round((_x select 2) / 16);
          };
          [[_x select 0,_x select 1,"483",_val],"life_fnc_wantedAdd",false,false] spawn life_fnc_MP;
           _names = _names + format["%1<br/>",_x select 1];
  } foreach _sellers;
 
hint parseText format["Les personnes suivantes ont vendues de la drogue :<br/><br/>%1",_names];
(_this select 0) setVariable["sellers",[],true];
life_action_inUse = false;
};