/*
	File: fn_searchClient.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Searches the player and he returns information back to the player.
	Edit by Lucel // altislife.fr
*/
private["_cop","_inv_legal","_inv_illegal","_var","_val","_robber","_item"];
_cop = [_this,0,Objnull,[objNull]] call BIS_fnc_param;
if(isNull _cop) exitWith {};
_inv_legal = [];
_inv_illegal = [];
_robber = false;
//legal items
{
	_item = _x select 0;
	_var = [_item,0] call life_fnc_varHandle;
	_val = missionNamespace getVariable _var;
	if(_val > 0) then
	{
		_inv_legal pushBack [_item,_val];
		//[false,_item,_val] call life_fnc_handleInv;
	};
} foreach life_legal_items;

{
	_item = _x select 0;
	_var = [_item,0] call life_fnc_varHandle;
	_val = missionNamespace getVariable _var;
	if(_val > 0) then
	{
		_inv_illegal pushBack [_item,_val];
		//[false,_item,_val] call life_fnc_handleInv;
	};
} foreach life_illegal_items;






if(!life_use_atm) then 
{
	life_flouze = 0;
	_robber = true;
};
//diag_log format ["Search _inv_legal %1",_inv_legal];
//diag_log format ["Search _inv_illegal %1",_inv_illegal];
[[player,_inv_legal,_robber,_inv_illegal],"life_fnc_copSearch",_cop,false] spawn life_fnc_MP;