#include <macro.h>
/*
	Master Life Configuration File
	This file is to setup variables for the client, there are still other configuration files in the system

*****************************
****** Backend Variables *****
*****************************
*/
life_query_time = time;
life_action_delay = time;
life_trunk_vehicle = Objnull;
life_session_completed = false;
life_garage_store = false;
life_session_tries = 0;
life_net_dropped = false;
life_hit_explosive = false;
life_siren_active = false;
life_siren2_active = false;
life_clothing_filter = 0;
life_clothing_uniform = -1;
life_redgull_effect = time;
life_is_processing = false;
life_bail_paid = false;
life_impound_inuse = false;
life_action_inUse = false;
life_spikestrip = ObjNull;
life_respawn_timer = 1; //Scaled in minutes
life_has_insurance = false;
life_knockout = false;
life_interrupted = false;
life_respawn_timer = 30;

life_autosave = true;
//life_death_loc = [0,0,0];
//Spawn point reb
life_reb_respawn_1 = true;
life_reb_respawn_2 = true;
life_reb_respawn_3 = true;
life_reb_respawn_4 = true;
life_reb_respawn_5 = true;

life_merc_respawn_1 = true;
life_merc_respawn_2 = true;

//Spawn point civ
life_civ_respawn_1 = true;
life_civ_respawn_2 = true;
life_civ_respawn_3 = true;
life_civ_respawn_4 = true;
life_civ_respawn_5 = true;
//Spawn point Cop
life_cop_respawn_1 = true;
life_cop_respawn_2 = true;
life_cop_respawn_3 = true;
life_cop_respawn_4 = true;
life_cop_respawn_5 = true;
life_cop_respawn_6 = true;
life_cop_respawn_7 = true;

life_respawned = false;
life_removeWanted = false;

life_fatigue = 0.5; //Set the max fatigue limit (50%)
//Persistent Saving
__CONST__(life_save_civ,TRUE); //Save weapons for civs?
__CONST__(life_save_yinv,TRUE); //Save Y-Inventory for civs and cops? (Medics excluded for now)

//Revive constant variables.
__CONST__(life_revive_cops,TRUE); //Set to false if you don't want cops to be able to revive downed players.
__CONST__(life_revive_fee,250); //Fee for players to pay when revived.
//House Limit
__CONST__(life_houseLimit,2); //Maximum amount of houses a player can buy (TODO: Make Tiered licenses).

//Gang related stuff?
__CONST__(life_gangPrice,150000); //Price for creating a gang (They're all persistent so keep it high to avoid 345345345 gangs).
__CONST__(life_gangUpgradeBase,10000); //MASDASDASD
__CONST__(life_gangUpgradeMultipler,2.5); //BLAH

__CONST__(life_enableFatigue,false); //Enable / Disable the ARMA 3 Fatigue System

//Uniform price (0),Hat Price (1),Glasses Price (2),Vest Price (3),Backpack Price (4)
life_clothing_purchase = [-1,-1,-1,-1,-1];

/*
*****************************
****** Extras *****
*****************************
*/

life_smartphoneTarget = ObjNull;
life_mauer = ObjNull;

/*
*****************************
****** Weight Variables *****
*****************************
*/
life_maxWeight = 24; //Identifies the max carrying weight (gets adjusted throughout game when wearing different types of clothing).
life_maxWeightT = 24; //Static variable representing the players max carrying weight on start.
life_carryWeight = 0; //Represents the players current inventory weight (MUST START AT 0).

/*
*****************************
****** Life Variables *******
*****************************
*/
life_net_dropped = false;
life_hit_explosive = false;
life_siren_active = false;
life_bank_fail = false;
life_use_atm = true;
life_is_arrested = false;
life_in_rea = false;
life_restrained = false;
life_delivery_in_progress = false;
life_action_in_use = false;
life_thirst = 100;
life_hunger = 100;
life_damage = 100;
__CONST__(life_paycheck_period,15);
life_flouze = 0;
__CONST__(life_impound_car,1500);
__CONST__(life_impound_boat,1500);
__CONST__(life_impound_air,3000);
life_istazed = false;
life_my_gang = ObjNull;
life_bidet = [0,0,0];
life_alive = false;
life_vehicles = [];
life_vehicles_tracked = [];
life_vehicles_c4ed = [];
bank_robber = [];
life_hudDisable = false;

//EPARGNE
life_epargne = 0;
switch (playerSide) do
{
	case west: 
	{
		life_dabflouze = 25000; //Starting Bank Money
		life_paye = 1000; //Paycheck Amount
	};
	case civilian: 
	{
		life_dabflouze = 25000; //Starting Bank Money
		life_paye = 2300; //Paycheck Amount
	};
};

/*
	Master Array of items?
*/
//life_vShop_rentalOnly = ["B_MRAP_01_hmg_F","B_G_Offroad_01_armed_F"];
//__CONST__(life_vShop_rentalOnly,life_vShop_rentalOnly); //These vehicles can never be bought and only 'rented'. Used as a balancer & money sink. If you want your server to be chaotic then fine.. Remove it..

life_inv_items = 
[
	"life_inv_oilu",
	"life_inv_oilp",
	"life_inv_houblon",
	"life_inv_houblonp",
	"life_inv_heroinu",
	"life_inv_heroinp",
	"life_inv_cannabis",
	"life_inv_marijuana",
	"life_inv_apple",
	"life_inv_pizza",
	"life_inv_glace",
	"life_inv_rabbit",
	"life_inv_salema",
	"life_inv_ornate",
	"life_inv_mackerel",
	"life_inv_tuna",
	"life_inv_mullet",
	"life_inv_catshark",
	"life_inv_turtle",
	"life_inv_fishingpoles",
	"life_inv_water",
	"life_inv_donuts",
	"life_inv_turtlesoup",
	"life_inv_coffee",
	"life_inv_fuelF",
	"life_inv_fuelE",
	"life_inv_pickaxe",
	"life_inv_copperore",
	"life_inv_ironore",
	"life_inv_ironr",
	"life_inv_copperr",
	"life_inv_sand",
	"life_inv_salt",
	"life_inv_saltr",
	"life_inv_glass",
	"life_inv_tbacon",
	"life_inv_lockpick",
	"life_inv_redgull",
	"life_inv_peach",
	"life_inv_diamond",
	"life_inv_coke",
	"life_inv_cokep",
	"life_inv_diamondr",
	"life_inv_spikeStrip",
	"life_inv_rock",
	"life_inv_cement",
	"life_inv_goldbar",
	"life_inv_menotte",
	"life_inv_blastingcharge",
	"life_inv_boltcutter",
	"life_inv_defusekit",
	"life_inv_storagesmall",
	"life_inv_storagebig",
	"life_inv_phos",
	"life_inv_soude",
	"life_inv_meth",
	"life_inv_mauer",
	"life_inv_trackerlucel",
	"life_inv_scannerlucel"
];

//Setup variable inv vars.
{missionNamespace setVariable[_x,0];} foreach life_inv_items;
//Licenses [license var, civ/cop]
life_licenses =
[
	["license_cop_air","cop"],
	["license_cop_swat","cop"],
	["license_cop_cg","cop"],
	["license_civ_driver","civ"],
	["license_civ_air","civ"],
	["license_civ_heroin","civ"],
	["license_civ_marijuana","civ"],
	["license_civ_gang","civ"],
	["license_civ_boat","civ"],
	["license_civ_oil","civ"],
	["license_civ_houblon","civ"],
	["license_civ_meth","civ"],
	["license_civ_dive","civ"],
	["license_civ_truck","civ"],
	["license_civ_piz","civ"],
	["license_civ_gla","civ"],	
	["license_civ_gun","civ"],
	["license_civ_rebel","civ"],
	["license_civ_coke","civ"],
	["license_civ_diamond","civ"],
	["license_civ_copper","civ"],
	["license_civ_iron","civ"],
	["license_civ_sand","civ"],
	["license_civ_salt","civ"],
	["license_civ_cement","civ"],
	["license_civ_dep","civ"],
	["license_civ_home", "civ"],
	["license_civ_taxi", "civ"],
	["license_civ_medic", "civ"],
	["license_civ_merc", "civ"],
	["license_civ_avocat", "civ"],
	["license_civ_pref", "civ"]
];

//Setup License Variables
{missionNamespace setVariable[(_x select 0),false];} foreach life_licenses;

life_dp_points = ["dp_1","dp_2","dp_3","dp_4","dp_5","dp_6","dp_7","dp_8","dp_9","dp_10","dp_11","dp_12","dp_13","dp_14","dp_15","dp_15","dp_16","dp_17","dp_18","dp_19","dp_20","dp_21","dp_22","dp_23","dp_24","dp_25"];
//[shortVar,reward]
life_illegal_items = [["cannabis",3400],["heroinu",3600],["heroinp",3600],["cocaine",3900],["cocainep",3900],["marijuana",3400],["turtle",5850],["meth",4500],["goldbar",10000],["blastingcharge",10000]];
life_legal_items = [
["salema",60],
["ornate",50],
["mackerel",60], // Maquereau
["tuna",400], // Thon
["mullet",200], // Mulet
["catshark",350], // ?
["rabbit",65], // Lapin	
["tbacon",25],	
["donuts",60],
["water",5], // Eau 
["coffee",5], 	
["redgull",200],
["fuelF",500], // jerricane d'essence 
["apple",50], // Pomme
["peach",55], // P�che
["iron_r",1500], // dif 2
["glass",1500], // 	dif 2
["biere",1800], // dif 3	
["cement",1500], // dif 2	
["salt_r",2200], // dif 4
["copper_r",2200], // dif 4
["oilp",1500], // dif 2
["diamondc",2200], // dif 3	
["turtlesoup",1000],
["lockpick",75], // Crochetage
["pickaxe",750], //Pioche
["spikeStrip",800], // Herse
["menotte",800],
["storagesmall",150000],
["storagebig",500000],
["mauer",2500],
["trackerlucel",2500],
["scannerlucel",2500],
["defusekit",2500]
];
life_trp_points = ["Pegasus_airport","Pegasus_1","Pegasus_2","Pegasus_3","Pegasus_4","Pegasus_5","Pegasus_6","Pegasus_7","Pegasus_1","Pegasus_2","Pegasus_4","Pegasus_5","Pegasus_6","Pegasus_7","Pegasus_7"];

/*
	Vente
*/
sell_array = 
[

	["salema",60],
	["ornate",50],
	["mackerel",60], // Maquereau
	["tuna",400], // Thon
	["mullet",200], // Mulet
	["catshark",350], // ?
	["pizza",150], // Pizza
	["glace",150], // Glace
	
	["rabbit",65], // Lapin	
	["tbacon",25],	
	["donuts",60],
	["water",5], // Eau 
	["coffee",5], 	
	["redgull",200],
	
	["fuelF",500], // jerricane d'essence 
	// Legal
	["apple",50], // Pomme
	["peach",55], // P�che
	["iron_r",1500], // dif 2
	["glass",1500], // 	dif 2
	["biere",1800], // dif 3	
	["cement",1500], // dif 2	
	["salt_r",2200], // dif 4
	["copper_r",2200], // dif 4
	["oilp",1500], // dif 2
	["diamondc",2200], // dif 3	
	//Illegal
	["cannabis",3400],
	["marijuana",3400], // Marijuana,  
	["heroinp",3600], // H�ro�ne trait�,  
	["heroinu",3600], // H�ro�ne trait�,  
	["cocainep",3900], // Coca�ne trait�, 	
	["cocaine",3900], // Coca�ne trait�, 	
	["turtle",5850], // Tortue,  	
	["meth",8000], // Meth  	
	
	["turtlesoup",1000],
	["lockpick",75], // Crochetage
	["pickaxe",750], //Pioche
	["spikeStrip",800], // Herse
	["menotte",800],
	["storagesmall",150000],
	["storagebig",500000],
	["mauer",2500],
	["blastingcharge",50000],
	["trackerlucel",5000],
	["scannerlucel",5000],

	["goldbar",95000]
];
__CONST__(sell_array,sell_array);

buy_array = 
[
	["apple",65],
	["peach",68],
	["salema",70],
	["ornate",60],
	["mackerel",70],
	["tuna",500],
	["mullet",3000],
	["catshark",450],
	
	["rabbit",75],
	["donuts",120],
	["tbacon",75],
	["redgull",1500],
	
	["coffee",10],
	["water",10],
	
	["turtlesoup",2500],
			
	["fuelF",850],
	
	["lockpick",1200],
	["pickaxe",1200],
	["menotte",900],
	["spikeStrip",1000],
	["storagesmall",150000],
	["storagebig",500000],
	["mauer",5000],
	["blastingcharge",250000],
	["boltcutter",7500],
	["defusekit",3500],
	["trackerlucel",10000],
	["scannerlucel",10000]
];
__CONST__(buy_array,buy_array);

life_weapon_shop_array =
[
	["acc_flashlight",0],
	["optic_Holosight",0],
	["optic_ACO_grn_smg",0],
	["optic_Aco_smg",0],
	["optic_ACO_grn",0],
	["optic_Aco",0],
	["acc_pointer_IR",0],					                                                
	["optic_Arco",0], 
	["optic_Hamr",0],
	["optic_MRCO",0],	
	["Binocular",0],
	["ItemMap",0],
	["ItemCompass",0],
	["ItemGPS",0],
	["ToolKit",0],
	["FirstAidKit",0],
	["Medikit",0],
	["SmokeShell",0],
	["NVGoggles",0],
	["NVGoggles_OPFOR",0],	
	["MineDetector",0],
	["Rangefinder",0],	
	["optic_MRD",0],	
	["B_UavTerminal",0],	
	["Chemlight_red",0],
	["Chemlight_yellow",0],
	["Chemlight_green",0],
	["SmokeShellPurple",0],
	["SmokeShellOrange",0],
	["Chemlight_blue",0],	
	["hgun_P07_snds_F",0],
	["hgun_P07_F",0],
	["hgun_ACPC2_F",0],
	["arifle_sdar_F",0],
	["SMG_02_F",0],
	["SMG_01_F",0],
	["arifle_MX_Black_F",0],
	["arifle_MXC_Black_F",0],
	["arifle_MXM_Black_F",0],				
	["srifle_EBR_F",0],
	["LMG_Mk200_F",0],
	["launch_RPG32_F",0],
	["arifle_TRG20_F",0],
	["arifle_Katiba_C_F",0],
	["srifle_DMR_01_F",0],
	["hgun_Rook40_F",0],
	["hgun_PDW2000_F",0],
	["arifle_Mk20C_plain_F",0],
	["16Rnd_9x21_Mag",0],
	["9Rnd_45ACP_Mag",0],
	["20Rnd_556x45_UW_mag",0],
	["30Rnd_9x21_Mag",0],
	["30Rnd_65x39_caseless_mag",0],
	["30Rnd_65x39_caseless_mag_Tracer",0],
	["200Rnd_65x39_cased_Box",0],
	["20Rnd_762x51_Mag",0],
	["RPG32_F",0],
	["30Rnd_556x45_Stanag",0],
	["10Rnd_762x51_Mag",0],
	["30Rnd_65x39_caseless_green",0],
	["arifle_TRG21_F",0],
	["muzzle_snds_L",0],
	["hgun_Pistol_heavy_02_F",0],
	["hgun_Pistol_heavy_01_F",nil,0],
	["11Rnd_45ACP_Mag",0],
	["6Rnd_45ACP_Cylinder",0],
	["30Rnd_45ACP_Mag_SMG_01",0]
];
__CONST__(life_weapon_shop_array,life_weapon_shop_array);

life_garage_prices =
[
	["O_Heli_Transport_04_bench_F",25000],
    ["O_Heli_Transport_04_covered_F",25000],
    ["O_Heli_Transport_04_medevac_F",25000],
    ["O_Heli_Transport_04_repair_F",25000],
    ["O_Heli_Transport_04_F",25000],
    ["B_Heli_Transport_03_unarmed_F",25000],
    ["C_Heli_Light_01_civil_F",15000],	
	
    ["C_Kart_01_Blu_F",1500],
    ["C_Kart_01_Fuel_F",1500],
    ["C_Kart_01_Vrana_F",1500],
    ["C_Kart_01_Red_F",1500],
	
    ["B_Quadbike_01_F",550],
    ["C_Hatchback_01_F",1500],
    ["C_Hatchback_01_sport_F",2350],
    ["C_Offroad_01_F",2500],
	["C_SUV_01_F",5250],
	
    ["C_Van_01_transport_F",7000],
    ["C_Van_01_box_F",7500],
	["C_Van_01_fuel_F",7000],
    ["I_Truck_02_transport_F",12500],
    ["I_Truck_02_covered_F",15000],
    ["I_Truck_02_box_F",5000],
    ["B_Truck_01_transport_F",25000],
    ["B_Truck_01_box_F", 30000],
    ["O_Truck_03_device_F", 30000], // Tempest
	
	
	["B_G_Offroad_01_F",3500],
	["B_G_Offroad_01_armed_F",10000],
	["O_MRAP_02_F",7500],
	
	["B_MRAP_01_F",7500],
	["B_MRAP_01_hmg_F",15000],
	["I_MRAP_03_F",12500],
	
    ["B_Heli_Light_01_F",15000],	
	["B_Heli_Attack_01_F",50000],
	["O_Heli_Light_02_unarmed_F",25000],
	["I_Heli_Transport_02_F",35000],
    ["I_Heli_light_03_unarmed_F",35000],
    ["B_Heli_Transport_01_F",50000],	

    ["C_Rubberboat",400],
	["I_G_Boat_Transport_01_F",1500],
    ["C_Boat_Civil_01_F",2500],
	
    ["B_Boat_Transport_01_F",400],
    ["C_Boat_Civil_01_police_F",2500],
    ["B_Boat_Armed_01_minigun_F",10000],
    ["B_SDV_01_F",2500]
    
];
__CONST__(life_garage_prices,life_garage_prices);

life_assur_prices =
[
	["O_Heli_Transport_04_bench_F",700000],
    ["O_Heli_Transport_04_covered_F",700000],
    ["O_Heli_Transport_04_medevac_F",700000],
    ["O_Heli_Transport_04_repair_F",700000],
    ["O_Heli_Transport_04_F",800000],
    ["B_Heli_Transport_03_unarmed_F",700000],
    ["C_Heli_Light_01_civil_F",150000],
	
	["C_Kart_01_Blu_F",5625],
    ["C_Kart_01_Fuel_F",5625],
    ["C_Kart_01_Vrana_F",5625],
    ["C_Kart_01_Red_F",5625],
	
	["B_Quadbike_01_F",1875],
	["C_Hatchback_01_F",5625],
	["C_Hatchback_01_sport_F",22500],
	["C_Offroad_01_F",9375],
	["C_SUV_01_F",13125],	
	
	["C_Van_01_transport_F",33750],
	["C_Van_01_box_F",41250],
	["C_Van_01_fuel_F",22500],
	["I_Truck_02_transport_F",75000],
	["I_Truck_02_covered_F",93750],
	["I_Truck_02_box_F",18750],
	["B_Truck_01_transport_F",198750],
	["B_Truck_01_box_F", 292500],	
	["O_Truck_03_device_F", 292500], // Tempest 

	["B_G_Offroad_01_F",13125],
	["B_G_Offroad_01_armed_F",281250],
	["O_MRAP_02_F",231250],
	
	["B_MRAP_01_F",20625],
	["B_MRAP_01_hmg_F",26250],
	["I_MRAP_03_F",22500],

	["B_Heli_Light_01_F",281250],	
	["B_Heli_Attack_01_F",375000],
	["O_Heli_Light_02_unarmed_F",375000],
	["I_Heli_Transport_02_F",562500],	
	["I_Heli_light_03_unarmed_F",562500],
	["B_Heli_Transport_01_F",375000],	
	
	["C_Rubberboat",1875],
	["I_G_Boat_Transport_01_F",5625],
	["C_Boat_Civil_01_F",8250],
	
	["B_Boat_Transport_01_F",1875],
	["C_Boat_Civil_01_police_F",8250],
	["B_Boat_Armed_01_minigun_F",18750],
	["B_SDV_01_F",18750]
];
__CONST__(life_assur_prices,life_assur_prices);

life_garage_sell =
[
	["O_Heli_Transport_04_bench_F",350000],
    ["O_Heli_Transport_04_covered_F",350000],
    ["O_Heli_Transport_04_medevac_F",350000],
    ["O_Heli_Transport_04_repair_F",350000],
    ["O_Heli_Transport_04_F",400000],
    ["B_Heli_Transport_03_unarmed_F",350000],
    ["C_Heli_Light_01_civil_F",300000],
	
    ["C_Kart_01_Red_F",5625],

	["C_Kart_01_Blu_F",5625],
    ["C_Kart_01_Fuel_F",5625],
    ["C_Kart_01_Vrana_F",5625],
    ["C_Kart_01_Red_F",5625],
	
	["B_Quadbike_01_F",1875],
	["C_Hatchback_01_F",5625],
	["C_Hatchback_01_sport_F",22500],
	["C_Offroad_01_F",9375],
	["C_SUV_01_F",13125],	
	
	["C_Van_01_transport_F",33750],
	["C_Van_01_box_F",41250],
	["C_Van_01_fuel_F",22500],
	["I_Truck_02_transport_F",75000],
	["I_Truck_02_covered_F",93750],
	["I_Truck_02_box_F",18750],
	["B_Truck_01_transport_F",198750],
	["B_Truck_01_box_F", 292500],	
	["O_Truck_03_device_F", 292500], // Tempest 

	["B_G_Offroad_01_F",13125],
	["B_G_Offroad_01_armed_F",281250],
	["O_MRAP_02_F",131250],
	
	["B_MRAP_01_F",20625],
	["B_MRAP_01_hmg_F",26250],
	["I_MRAP_03_F",22500],

	["B_Heli_Light_01_F",281250],	
	["B_Heli_Attack_01_F",375000],
	["O_Heli_Light_02_unarmed_F",375000],
	["I_Heli_Transport_02_F",562500],	
	["I_Heli_light_03_unarmed_F",562500],
	["B_Heli_Transport_01_F",375000],	
	
	["C_Rubberboat",1875],
	["I_G_Boat_Transport_01_F",5625],
	["C_Boat_Civil_01_F",8250],
	
	["B_Boat_Transport_01_F",1875],
	["C_Boat_Civil_01_police_F",8250],
	["B_Boat_Armed_01_minigun_F",18750],
	["B_SDV_01_F",18750]
];
__CONST__(life_garage_sell,life_garage_sell);