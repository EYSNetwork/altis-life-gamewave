#include <macro.h>
/*
	File: fn_onRespawn.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Execute various actions when the _unit respawns.
*/
private["_unit","_corpse","_handle","_spawn"];
_unit = [_this,0,objNull,[objNull]] call BIS_fnc_param;
_corpse = [_this,1,objNull,[objNull]] call BIS_fnc_param;
if(isNull _unit) exitWith {};
if(!isNull _corpse) then{deleteVehicle _corpse;};

hideBody _corpse;
deleteVehicle _corpse;
_unit setVariable["steam64id",(getPlayerUID player),true]; //Reset the UID.
_unit setVariable["realname",profileName,true]; //Reset the players name.



_handle = [] spawn life_fnc_setupActions;
waitUntil {scriptDone _handle};



switch(playerSide) do
{
	case west: 
	{
		if (_unit getVariable "imMedicBro") then {
		_handle = [] spawn life_fnc_copmedicLoadout;
		}else{
		_handle = [] spawn life_fnc_loadGear;
		};
		_unit setVariable["CopRestrain",false,true];
		_unit setVariable["restrained",false,true];
		_unit setVariable["Escorting",false,true];
		_unit setVariable["transporting",false,true];
		_unit setVariable["surrender",false,true];
		_unit addRating 9999998;
	};
	
	case civilian:
	{
		_handle = [] spawn life_fnc_civLoadout;
		_unit setVariable["CopRestrain",false,true];
		_unit setVariable["restrained",false,true];
		_unit setVariable["Escorting",false,true];
		_unit setVariable["transporting",false,true];
		_unit setVariable["surrender",false,true];
		//if(headGear player != "") then {removeHeadgear player;};
		//if(goggles player != "") then {removeGoggles player;};
		_unit addRating 9999999;
	};
	waitUntil {scriptDone _handle};
};

if(life_is_arrested) then
{
	hint "Pas de suicide en PRISON ! Tu y retourne mon petit !";
	life_is_arrested = false;
	[_unit,true] spawn life_fnc_jail;
}
	else
{
		
	titleText["","BLACK FADED"];
	titleFadeOut 9999999999;
	[] call life_fnc_spawnMenu;
	waitUntil{isNull (findDisplay 38500)}; //Wait for the spawn selection to be done.
};



//[[_unit,life_sidechat,playerSide],"TON_fnc_managesc",false,false] spawn life_fnc_MP;
[] call life_fnc_hudUpdate;
cutText ["","BLACK IN"];

//[] call life_fnc_civFetchGear;
[] spawn FAR_Player_Init;
//nul = player execVM "scripts\lock_backpack.sqf";
//[] call life_fnc_setupActions;
[] call life_fnc_updateClothing;
[] execVM "zlt_fastrope.sqf";
player enableFatigue (__GETC__(life_enableFatigue));
