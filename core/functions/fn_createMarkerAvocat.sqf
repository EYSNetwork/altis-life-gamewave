/*
	File: fn_createMarker.sqf
	Author: Skalicon
	
	Description:
	Adds a maker to the map locally
*/
if(!(license_civ_medic)) exitWith {};
Private["_playerName","_pos","_marker","_text"];
if(isServer) exitWith {};
_playerName = [_this,0,"",[""]] call BIS_fnc_param;
_prefix = [_this,2,"-AVOCAT-",["-AVOCAT-"]] call BIS_fnc_param;
_pos = _this select 1;
_text = format["%2 %1 -AVOCAT-", _playerName, _prefix];
_playerName = format["%2%1", _playerName, _prefix];
deleteMarkerLocal _playerName;
_marker = createMarkerLocal [_playerName, _pos];
_marker setMarkerShapeLocal "ICON";
_marker setMarkerTypeLocal "mil_dot";
_marker setMarkerColorLocal "ColorWhite";
_marker setMarkerTextLocal _text;

//_marker setVariable ["DEPLUCEL", true, true];
/*
if(license_civ_dep) then {
waitUntil{(visibleMap)}
_marker setMarkerAlphaLocal 1;
};
waitUntil{!(visibleMap)}
_marker setMarkerAlphaLocal 0;
*/
diag_log format ["MARKERDEP || playername: %1 _pos :%2 _marker : %3", _playerName, _pos, _marker];
sleep 300;
deleteMarkerLocal _playerName;
