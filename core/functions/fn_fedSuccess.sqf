/*
	File: fn_fedSuccess.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Ugh coming up with a name for it was stupid but whatever.
	If the robbery was successful on the Federal reserve then punish
	everyone and take some money.

	Support for Bank Insurance is there.
*/
private["_funds"];
_funds = [_this,0,-1,[0]] call BIS_fnc_param;
if(_funds == -1) exitWith {};
if(!life_use_atm) exitWith {};
sleep 45;

if(_funds >= life_dabflouze && !life_has_insurance) then
{
	hint "La banque s'est fait dévaliser et vous êtes a 0 € ! .\n\nLa prochaine fois pensez à prendre une assurance Banquaire !";
	life_dabflouze = 0;
}
	else
{
	if(life_has_insurance) then
	{
		hint "La banque d'Altis a du taxer la population pour les frais de braquage... Vous ne perdez rien vu que vous avez pris l'assurance Banquaire";
		life_has_insurance = false;
	}
		else
	{
		hint format["De dangereux criminel ont volé la banque d'Altis ! Nous devons taxer %1€ directement de votre compte pour combler les frais.\n\nLa prochaine fois pensez à prendre une assurance Banquaire !",[_funds] call life_fnc_numberText];
		life_dabflouze = life_dabflouze - _funds;
	};
};