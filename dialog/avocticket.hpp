#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Prospere Lucel, v1.063, #Fyheqe)
////////////////////////////////////////////////////////

class Life_avocat_ticket_give
{
	idd = 2650;
	name = "life_avocat_ticket_give";
	movingEnabled = false;
	enableSimulation = true;
	
	class controlsBackground {
		class Life_RscTitleBackground:Life_RscText {
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			idc = -1;
	x = 6.3 * GUI_GRID_W + GUI_GRID_X;
	y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 27.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
		};
		
		class MainBackground:Life_RscText {
			colorBackground[] = {0, 0, 0, 0.7};
			idc = -1;
	x = 6.3 * GUI_GRID_W + GUI_GRID_X;
	y = 5.9 * GUI_GRID_H + GUI_GRID_Y;
	w = 27.5 * GUI_GRID_W;
	h = 6.5 * GUI_GRID_H;
		};
	};	
	class controls 
	{
		class Title : Life_RscTitle {
			colorBackground[] = {0, 0, 0, 0};
			idc = 2651;
			text = "Facturation";
	x = 6.5 * GUI_GRID_W + GUI_GRID_X;
	y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 27 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class moneyEdit : Life_RscEdit 
		{
			idc = 2652;
			
			text = "500";
			sizeEx = 0.030;
	x = 16 * GUI_GRID_W + GUI_GRID_X;
	y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 7.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
		};

		class payTicket: Life_RscButtonMenu {
			idc = -1;
			text = "Présenter la facture";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			onButtonClick = "[] call life_fnc_avocat_ticketGive; closeDialog 0;";
	x = 13 * GUI_GRID_W + GUI_GRID_X;
	y = 10 * GUI_GRID_H + GUI_GRID_Y;
	w = 12 * GUI_GRID_W;
	h = 2 * GUI_GRID_H;
	tooltip="Demander un paiement à un client. Attention si votre client refuse de payer il sera signalé automatiquement aux forces de Police !";

		};
			class RscText_1005: RscText
{
	idc = 2651;

	text = "Prix :"; //--- ToDo: Localize;
	x = 13 * GUI_GRID_W + GUI_GRID_X;
	y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 3 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
	colorText[] = {0.95,0.95,0.95,1};
};
	class RscText_1006: RscText
{
	idc = 2651;

	text = "€"; //--- ToDo: Localize;
	x = 24 * GUI_GRID_W + GUI_GRID_X;
	y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 1.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
	colorText[] = {0.95,0.95,0.95,1};
};
	};
};

class Life_avocat_ticket_pay
{
	idd = 2600;
	name = "life_avocat_ticket_pay";
	movingEnabled = false;
	enableSimulation = true;
	
	class controlsBackground {
		class Life_RscTitleBackground:Life_RscText {
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			idc = -1;
	x = 7.38 * GUI_GRID_W + GUI_GRID_X;
	y = 3.15 * GUI_GRID_H + GUI_GRID_Y;
	w = 27.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class MainBackground:Life_RscText {
			colorBackground[] = {0, 0, 0, 0.7};
			idc = -1;
	x = 7.3 * GUI_GRID_W + GUI_GRID_X;
	y = 4.1 * GUI_GRID_H + GUI_GRID_Y;
	w = 27.5 * GUI_GRID_W;
	h = 8 * GUI_GRID_H;
		};
	};
	
	class controls 
	{
		class InfoMsg : Life_RscStructuredText
		{
			idc = 2601;
			sizeEx = 0.020;
			text = "";
	x = 8.5 * GUI_GRID_W + GUI_GRID_X;
	y = 5 * GUI_GRID_H + GUI_GRID_Y;
	w = 25 * GUI_GRID_W;
	h = 3.5 * GUI_GRID_H;
		};

		class payTicket: Life_RscButtonMenu {
			idc = -1;
			text = "Payer la facture";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			onButtonClick = "[] call life_fnc_avocat_ticketPay;";
	x = 8.5 * GUI_GRID_W + GUI_GRID_X;
	y = 9 * GUI_GRID_H + GUI_GRID_Y;
	w = 25 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class refuseTicket : Life_RscButtonMenu {
			idc = -1;
			text = "Refuser la facture";
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			onButtonClick = "closeDialog 0;";
	x = 8.5 * GUI_GRID_W + GUI_GRID_X;
	y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 25 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
	};
};