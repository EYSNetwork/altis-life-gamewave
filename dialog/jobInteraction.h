#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)


class jobInteraction_Menu
{
	idd = 37400;
	movingEnabled = false;
	enableSimulation = true;
	
	class controlsBackground
	{
		class RscTitleBackground:life_RscText 
		{
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", "(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
			idc = -1;
	x = 9.5 * GUI_GRID_W + GUI_GRID_X;
	y = 4.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 21.5 * GUI_GRID_W;
	h = 1.5 * GUI_GRID_H;
		};
		
		class MainBackground : life_RscText
		{
			idc = -1;
			colorBackground[] = {0,0,0,0.7};
	x = 9.5 * GUI_GRID_W + GUI_GRID_X;
	y = 6 * GUI_GRID_H + GUI_GRID_Y;
	w = 21.5 * GUI_GRID_W;
	h = 13 * GUI_GRID_H;
		};
		
		class Title : life_RscTitle
		{
			colorBackground[] = {0,0,0,0};
			idc = 37401;
			text = "Menu métier civil";
	x = 14 * GUI_GRID_W + GUI_GRID_X;
	y = 5 * GUI_GRID_H + GUI_GRID_Y;
	w = 13.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
	};
	
	class controls
	{
		class ButtonClose : life_RscButtonMenu 
		{
			idc = -1;
			//shortcuts[] = {0x00050000 + 2};
			text = "$STR_Global_Close";
			onButtonClick = "closeDialog 0;";
	x = 9.5 * GUI_GRID_W + GUI_GRID_X;
	y = 19 * GUI_GRID_H + GUI_GRID_Y;
	w = 21.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonOne : life_RscButtonMenu
		{
			idc = 37450;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 6.7 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonTwo : life_RscButtonMenu
		{
			idc = 37451;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.3 * GUI_GRID_W + GUI_GRID_X;
	y = 7.95 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonThree : life_RscButtonMenu
		{
			idc = 37452;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 9.2 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonFour : life_RscButtonMenu
		{
			idc = 37453;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 10.45 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonFive : life_RscButtonMenu
		{
			idc = 37454;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 11.7 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonSix : life_RscButtonMenu
		{
			idc = 37455;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 15.45 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		
		class ButtonSeven : life_RscButtonMenu
		{
			idc = 37456;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 12.95 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		class ButtonEight : life_RscButtonMenu
		{
			idc = 37457;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 14.2 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
		class ButtonNine : life_RscButtonMenu
		{
			idc = 37458;
			colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])", "(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])", "(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])", 0.5};
			text = "";
			sizeEx = 0.025;
	x = 12.2 * GUI_GRID_W + GUI_GRID_X;
	y = 16.75 * GUI_GRID_H + GUI_GRID_Y;
	w = 16.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
		};
	};
};
