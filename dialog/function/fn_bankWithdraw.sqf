/*
	COPY PASTE TIME
*/
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_val"];
_val = parseNumber(ctrlText 2702);
if(_val > 999999) exitWith {hint "Attention vous ne pouvez pas dépasser le plafond de retrait : 999 999 €";};
if(_val < 0) exitwith {};
if(!([str(_val)] call life_fnc_isnumeric)) exitWith {hint "Je ne suis qu'un distributeur..."};
if(_val > life_dabflouze) exitWith {hint "Retrait refusé."};
if(_val < 100 && life_dabflouze > 20000000) exitWith {hint "Désolé mais votre banque n'autorise que les retraits de plus de 100€."}; //Temp fix for something.

life_flouze = life_flouze + _val;
life_dabflouze = life_dabflouze - _val;
hint format ["Vous avez retiré %1€ de votre compte bancaire",[_val] call life_fnc_numberText];
[] call life_fnc_atmMenu;

[6] call SOCK_fnc_updatePartial;

//[[getPlayerUID player,playerSide,life_flouze,6,life_dabflouze],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;
//[] call SOCK_fnc_updateRequest; //Silent Sync