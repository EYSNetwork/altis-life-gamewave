/*
	File: fn_bankTransfer.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Figure it out again.
*/
if((time - life_action_delay) < 5) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_val","_unit","_tax","_idGive","_idReceive"];
_val = parseNumber(ctrlText 2702);
_unit = call compile format["%1",(lbData[2703,(lbCurSel 2703)])];
if(isNull _unit) exitWith {};
_idGive = getPlayerUID player;
_idReceive = getPlayerUID _unit;
if((lbCurSel 2703) == -1) exitWith {hint "Vous devez choisir une personne pour pouvoir transférer."};
if(isNil "_unit") exitWith {hint "Le joueur sélectionné, n'est plus de ce monde."};
if(_val > 999999) exitWith {hint "Vous ne pouvez transférer plus de 999,999€";};
if(_val < 0) exitwith {};
if(!([str(_val)] call life_fnc_isnumeric)) exitWith {hint "Vous devez rentrer des chiffres !"};
if(_val > life_dabflouze) exitWith {hint "Vous n'avez pas tant d'argent sur votre compte bancaire !"};
_tax = [_val] call life_fnc_taxRate;
if((_val + _tax) > life_dabflouze) exitWith {hint format["Vous n'avez pas tant d'argent sur votre compte bancaire, pour tranférer %1€ vous avez besoin de %2€ de plus comme taxe bancaire.",_val,_tax]};

life_dabflouze = life_dabflouze - (_val + _tax);

[[_val,profileName,_idGive,_idReceive],"TON_fnc_clientWireTransfer",_unit,false] spawn life_fnc_MP;

[] call life_fnc_atmMenu;
hint format["vous avez tranférer %1€ à %2.\n\nUne taxe bancaire de %3€ a été prévelée comme taxe bancaire.",[_val] call life_fnc_numberText,_unit getVariable["realname",name _unit],[_tax] call life_fnc_numberText];
//[] call SOCK_fnc_updateRequest; //Silent Sync
//[[getPlayerUID player,playerSide,life_flouze,6,life_dabflouze],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;
[1] call SOCK_fnc_updatePartial;
