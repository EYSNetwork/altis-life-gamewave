/*
	File: fn_bankDeposit.sqf
	Author: Bryan "Tonic" Boardwine
	
	Description:
	Figure it out.
*/
if((time - life_action_delay) < 2) exitWith {hint "Vous appuyez trop vite !"};
life_action_delay = time;
private["_value"];
_value = parseNumber(ctrlText 2702);

//Series of stupid checks
if(_value > 999999) exitWith {hint localize "STR_ATM_GreaterThan";};
if(_value < 0) exitWith {};
if(!([str(_value)] call life_fnc_isnumeric)) exitWith {hint localize "STR_AIM_notnumeric"};
if(_value > life_flouze) exitWith {hint "Tu n'as pas tant d'argent sur toi !"};

life_flouze = life_flouze - _value;
life_dabflouze = life_dabflouze + _value;

hint format["Vous avez déposé %1€ dans votre compte en banque.",[_value] call life_fnc_numberText];
[] call life_fnc_atmMenu;

[6] call SOCK_fnc_updatePartial;

// Sync life_flouze to DB:
//[[getPlayerUID player,playerSide,life_flouze,6,life_dabflouze],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;

// Sync life_dabflouze to DB:
//[[getPlayerUID player,playerSide,life_dabflouze,1],"DB_fnc_updatePartial",false,false] spawn life_fnc_MP;
//[] call SOCK_fnc_updateRequest; //Silent Sync