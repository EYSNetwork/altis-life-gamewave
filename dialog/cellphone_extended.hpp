#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)

class cellphone_extended {
	idd = 3000;
	name= "cellphone_extended";
	movingEnable = false;
	enableSimulation = true;
	onLoad = "[] spawn life_fnc_cellex_open";
	
	class controlsBackground
	{
		class RscStructuredText_1100: RscStructuredText
		{
			idc = 1100;
			text = "<t align='center' size='1.2'>Téléphone</t>";
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = -0.5 * GUI_GRID_H + GUI_GRID_Y;
			w = 32 * GUI_GRID_W;
			h = 1.5 * GUI_GRID_H;
			colorBackground[] = {0,0,0,0.8};
		};
		class IGUIBack_2200: IGUIBack
		{
			idc = 2200;
		x = 4 * GUI_GRID_W + GUI_GRID_X;
		y = 1 * GUI_GRID_H + GUI_GRID_Y;
		w = 32 * GUI_GRID_W;
		h = 20 * GUI_GRID_H;
		};
	};
	
	class controls 
	{
		class bClose: RscButtonMenu
	    {
	    	idc = 2400;
	    	text = "Fermer"; 
			x = 4 * GUI_GRID_W + GUI_GRID_X;
			y = 21 * GUI_GRID_H + GUI_GRID_Y;
			w = 32 * GUI_GRID_W;
			h = 1 * GUI_GRID_H;
			onButtonClick="closeDialog 0;";
	    };
	    class lbDst: RscListbox
	    {
	    	idc = 1500;
	x = 5 * GUI_GRID_W + GUI_GRID_X;
	y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 12.5 * GUI_GRID_W;
	h = 10 * GUI_GRID_H;
			sizeEx = 0.030;
	    };
	    class editMessage: Life_RscEdit
	    {
	    	idc = 1400;
	x = 5 * GUI_GRID_W + GUI_GRID_X;
	y = 2.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 30 * GUI_GRID_W;
	h = 7.5 * GUI_GRID_H;
	    	text="";
			style = 16;
	    };
	    class bSend: RscButtonMenu
		{
			idc = 2401;
			text = "Envoi du message"; 
	x = 21.62 * GUI_GRID_W + GUI_GRID_X;
	y = 11 * GUI_GRID_H + GUI_GRID_Y;
	w = 13.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
			onButtonClick="[false] spawn life_fnc_cellex_sendClick;";
			tooltip="Envoyer un message.";
		};
		class bSendAdmin: RscButtonMenu
		{
			idc = 2402;
			text = "Envoi du message (ADMIN)"; 
	x = 21.62 * GUI_GRID_W + GUI_GRID_X;
	y = 12 * GUI_GRID_H + GUI_GRID_Y;
	w = 13.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
			onButtonClick="[true] spawn life_fnc_cellex_sendClick;";
			tooltip="Envoyez un mail en tant qu'administrateur. Fonctionne uniquement pour les messages privés à un joueur.";
		};	
		class bSendPos: RscButtonMenu
		{
			idc = 2403;
			text = "Envoyer ma position via GPS";
	x = 19.5 * GUI_GRID_W + GUI_GRID_X;
	y = 18 * GUI_GRID_H + GUI_GRID_Y;
	w = 15.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
			onButtonClick="[] spawn life_fnc_cellex_sendPosLucel;";
			tooltip="Envoyer votre position sur la carte à un contact.";
		};
		class RscButtonMenu_2404: RscButtonMenu
		{
			idc = 2402;
			onButtonClick = "createDialog ""Life_my_smartphone"";";
			text = "MESSAGERIE";
	x = 28.66 * GUI_GRID_W + GUI_GRID_X;
	y = 1.25 * GUI_GRID_H + GUI_GRID_Y;
	w = 6.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
			tooltip = "Consultez vos message personnel"; 
		};
		class RscButtonMenu_2405: RscButtonMenu
		{
			idc = 13378;
			onButtonClick = "[5] call life_fnc_smartphone;";

			text = "Réponse Rapide"; //--- ToDo: Localize;
	x = 19.5 * GUI_GRID_W + GUI_GRID_X;
	y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
	w = 15.5 * GUI_GRID_W;
	h = 1 * GUI_GRID_H;
			colorBackground[] = {0,0,0,0.8};
			tooltip = "Répondre a votre dernier contact"; //--- ToDo: Localize;
		};
	};
};