#define GUI_GRID_X	(0)
#define GUI_GRID_Y	(0)
#define GUI_GRID_W	(0.025)
#define GUI_GRID_H	(0.04)
#define GUI_GRID_WAbs	(1)
#define GUI_GRID_HAbs	(1)
////////////////////////////////////////////////////////
// GUI EDITOR OUTPUT START (by Gilles Tuaillon, v1.063, #Gyfyri)
////////////////////////////////////////////////////////
class lucelmove_Menu
{
	idd = 37400;
	movingEnabled = false;
	enableSimulation = true;
	
	class RscTitleBackground: Life_RscText
	{
		idc = -1;

		x = 14.5 * GUI_GRID_W + GUI_GRID_X;
		y = 4 * GUI_GRID_H + GUI_GRID_Y;
		w = 12 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])","(profilenamespace getvariable ['GUI_BCG_RGB_A',0.7])"};
	};
	class MainBackground: Life_RscText
	{
		idc = -1;

		x = 7.5 * GUI_GRID_W + GUI_GRID_X;
		y = 5.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 25 * GUI_GRID_W;
		h = 16.5 * GUI_GRID_H;
		colorBackground[] = {0,0,0,0.7};
	};
	class Title: Life_RscTitle
	{
		idc = 37401;

		text = "Menu d'intéraction du joueur"; //--- ToDo: Localize;
		x = 14.5 * GUI_GRID_W + GUI_GRID_X;
		y = 4 * GUI_GRID_H + GUI_GRID_Y;
		w = 12 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {0.95,0.95,0.95,1};
	};
	class ButtonClose: Life_RscButtonMenu
	{
		onButtonClick = "closeDialog 0;";

		idc = 1003;
		text = "Fermer"; //--- ToDo: Localize;
		x = 8 * GUI_GRID_W + GUI_GRID_X;
		y = 22 * GUI_GRID_H + GUI_GRID_Y;
		w = 6.25 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {0,0,0,0.8};
	};
	class ButtonOne: Life_RscButtonMenu
	{
		idc = 37450;
		onButtonClick = "player switchMove AmovPercMstpSnonWnonDnon_exercisePushup;";
		text = "Entrainement ! 5 POMPES !";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 7.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonTwo: Life_RscButtonMenu
	{
		idc = 37451;
		onButtonClick = "player switchMove AmovPercMstpSnonWnonDnon_exercisekneeBendB;";
		text = "Entrainement ! hop hop!";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 9 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonThree: Life_RscButtonMenu
	{
		idc = 37452;
		onButtonClick = "player switchMove Acts_TreatingWounded01;";
		text = "Fake massage cardiaque";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 10.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonFour: Life_RscButtonMenu
	{
		idc = 37453;
		onButtonClick = "player switchMove Acts_SignalToCheck;";
		text = "C'est par là monsieur !";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 12 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonFive: Life_RscButtonMenu
	{
		idc = 37454;
		onButtonClick = "player switchMove Acts_ShowingTheRightWay_loop;";
		text = "Circulez ! circulez !";	
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 13.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonSix: Life_RscButtonMenu
	{
		idc = 37455;
		onButtonClick = "player switchMove Acts_PercMstpSlowWrflDnon_handup2c;";
		text = "Stop on ne bouge plus !";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 18 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonSeven: Life_RscButtonMenu
	{
		idc = 37456;
		onButtonClick = "player switchMove Acts_PercMstpSlowWrflDnon_handup1c;";
		text = "Salut de loin";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 15 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonEight: Life_RscButtonMenu
	{
		idc = 37457;
		onButtonClick = "player switchMove AmovPercMstpSnonWnonDnon_exerciseKata;";
		text = "KARATE !!!";

		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 16.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	class ButtonNine: Life_RscButtonMenu
	{
		idc = 37458;
		onButtonClick = "player switchMove Acts_listeningToRadio_Loop;";
		text = "Listen Radio Looop";
		x = 10 * GUI_GRID_W + GUI_GRID_X;
		y = 19.5 * GUI_GRID_H + GUI_GRID_Y;
		w = 20 * GUI_GRID_W;
		h = 1 * GUI_GRID_H;
		colorText[] = {1,1,1,1};
		colorBackground[] = {"(profilenamespace getvariable ['GUI_BCG_RGB_R',0.3843])","(profilenamespace getvariable ['GUI_BCG_RGB_G',0.7019])","(profilenamespace getvariable ['GUI_BCG_RGB_B',0.8862])",0.5};
	};
	////////////////////////////////////////////////////////
	// GUI EDITOR OUTPUT END
	////////////////////////////////////////////////////////
}: