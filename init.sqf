#define DEBUG false
enableSaving [false, false];
X_Server = false;
X_Client = false;
X_JIP = false;
StartProgress = false;
introSpawn = nil;
RscSpectator_allowFreeCam = false;
if(!isDedicated) then { X_Client = true;};
life_versionInfo = "Altis Life RPG v3.1.4.8";
[] execVM "briefing.sqf"; //Load Briefing
[DEBUG] call compile preprocessFileLineNumbers "globalCompile.sqf";
onPlayerDisconnected { [_id, _name, _uid] call compile preProcessFileLineNumbers "core\functions\fn_onPlayerDisconnect.sqf" };
[] execVM "KRON_Strings.sqf";
_igiload = execVM "IgiLoad\IgiLoadInit.sqf";
[] execVM "zlt_fastrope.sqf";
[] execVM "scripts\meteo.sqf";

if (!isDedicated) then
{
	[] spawn
	{
		if (hasInterface) then // Normal player
		{
		[] execVM "fpsFix\vehicleManager.sqf";
		}
		else // Headless
		{
			waitUntil {!isNull player};
			if (typeOf player == "HeadlessClient_F") then
			{
				execVM "headless\init.sqf";
			};
		};
	};
};


call compileFinal preprocessFileLineNumbers "FAR_revive\FAR_revive_init.sqf";

StartProgress = true;

{_x setVariable ["BIS_noCoreConversations", true]} forEach allUnits;
0 fadeRadio 0;
enableSentences false;




